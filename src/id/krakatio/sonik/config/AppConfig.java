/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.config;

import javafx.scene.image.Image;

/**
 *
 * @author Deri Armanda
 */
public class AppConfig {
    
    public static final String APP_NAME = "Sonik App";
    public static final String APP_PACKAGE = "/id/krakatio/sonik/";
    public static final Image APP_LOGO = new Image(APP_PACKAGE+"icon/logo_app.png");
    
}
