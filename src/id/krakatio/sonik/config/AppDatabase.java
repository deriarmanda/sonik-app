/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.config;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import id.krakatio.sonik.util.DialogBuilder;
import java.io.File;
import java.sql.SQLException;

/**
 *
 * @author Deri Armanda
 */
public class AppDatabase {
    
    private static final String TAG_NAME = AppDatabase.class.getSimpleName();
    private static final String DB_NAME = "sonik.db";
    private static final String DB_PATH = 
            System.getProperty("user.dir")+File.separator+DB_NAME;
    private static final String JDBC_CONFIG = "jdbc:sqlite:" + DB_PATH;
    
    private static ConnectionSource conn;
        
    
    public static ConnectionSource getConnection() {
        if (conn == null) try {
            conn = new JdbcConnectionSource(JDBC_CONFIG);
        } catch (SQLException ex) {
            conn = null;
            System.err.println("Error while creating db source: "+ex);
            DialogBuilder.showError("Gagal menyambungkan ke database.");
        }
        return conn;
    }
    
}
