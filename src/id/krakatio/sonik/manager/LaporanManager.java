/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.manager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import id.krakatio.sonik.config.AppDatabase;
import id.krakatio.sonik.model.Laporan;
import id.krakatio.sonik.util.DialogBuilder;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Deri Armanda
 */
public class LaporanManager {    
    
    private static LaporanManager SINGLETON;
    private final Dao<Laporan, Integer> laporanDao;
    
    private LaporanManager() throws SQLException {
        laporanDao = DaoManager.createDao(
                AppDatabase.getConnection(), 
                Laporan.class
        );
    }
    
    public static void init() {
        try {
            SINGLETON = new LaporanManager();
        } catch (SQLException ex) {
            SINGLETON = null;
            System.err.println("Error while creating Laporan DAO: "+ex);
            DialogBuilder.showError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public static LaporanManager getInstance() {
        if (SINGLETON == null) init();
        return SINGLETON;
    }
    
    public void insertLaporan(Laporan laporan, ICallback<String> callback) {
        try {
            laporanDao.create(laporan);
            callback.onSuccess("Data Laporan berhasil disimpan.");
        } catch (SQLException ex) {
            System.err.println("Error while insert Laporan with uid ("+laporan.getUid()+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public void updateLaporan(Laporan laporan, ICallback<String> callback) {
        try {
            laporanDao.update(laporan);
            callback.onSuccess("Data Laporan berhasil dirubah.");
        } catch (SQLException ex) {
            System.err.println("Error while update Laporan with uid ("+laporan.getUid()+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public void deleteLaporan(Laporan laporan, ICallback<String> callback) {
        try {
            laporanDao.delete(laporan);
            callback.onSuccess("Data Laporan berhasil dihapus.");
        } catch (SQLException ex) {
            System.err.println("Error while delete Laporan with uid ("+laporan.getUid()+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public void getLaporanList(ICallback<List<Laporan>> callback) {
        try {
            List<Laporan> list = laporanDao.queryForAll();
            callback.onSuccess(list);
        } catch (SQLException ex) {
            System.err.println("Error while load all Laporan from Db: "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public void findLaporanByValue(String value, ICallback<Laporan> callback) {
        try {
            List<Laporan> list = laporanDao.queryForAll();
            for (Laporan l : list) {
                if (l.contains(value)) {
                    callback.onSuccess(l);
                    return;
                }
            }
        } catch (SQLException ex) {
            System.err.println("Error while load all Laporan from Db: "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    private static final String GENERAL_ERROR_MESSAGE = 
            "Terjadi kesalahan ketika mengakses tabel Laporan di database. Silahkan menghubungi admin.";
}
