/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.manager;

/**
 *
 * @author Deri Armanda
 * @param <T>
 */
public interface ICallback<T> {
    void onSuccess(T data);
    void onError(String message);
}
