/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.manager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import id.krakatio.sonik.config.AppDatabase;
import id.krakatio.sonik.model.Attachment;
import id.krakatio.sonik.model.Peminjam;
import id.krakatio.sonik.model.Surat;
import id.krakatio.sonik.util.DialogBuilder;
import id.krakatio.sonik.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Deri Armanda
 */
public class SuratManager {
    
    private static SuratManager SINGLETON;
    private final Dao<Surat, Integer> suratDao;
    private final Dao<Attachment, Integer> attachmentDao;
    private final Dao<Peminjam, Integer> peminjamDao;
    private final ObservableList<Surat> suratList;
    
    private SuratManager() throws SQLException {
        suratDao = DaoManager.createDao(
                AppDatabase.getConnection(), 
                Surat.class
        );
        attachmentDao = DaoManager.createDao(
                AppDatabase.getConnection(), 
                Attachment.class
        );
        peminjamDao = DaoManager.createDao(
                AppDatabase.getConnection(), 
                Peminjam.class
        );
        suratList = FXCollections.observableArrayList();
    }
    
    public static void init() {
        try {
            SINGLETON = new SuratManager();
        } catch (SQLException ex) {
            SINGLETON = null;
            System.err.println("Error while creating Surat DAO: "+ex);
            DialogBuilder.showError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public static SuratManager getInstance() {
        if (SINGLETON == null) init();
        return SINGLETON;
    }
    
    public void getSuratList(
            boolean isRefreshed, 
            ICallback<ObservableList<Surat>> callback
    ) {
        if (!isRefreshed && !suratList.isEmpty()) {
            callback.onSuccess(suratList);
        } else {
            try {
                loadSuratList();
                callback.onSuccess(suratList);
            } catch (SQLException ex) {
                System.err.println("Error while load all Surat from Db: "+ex);
                callback.onError(GENERAL_ERROR_MESSAGE);
            }
        }
    }
    
    public void findSurat(Surat surat, ICallback<Surat> callback) {
        try {
            List<Surat> founds = suratDao.queryForMatching(surat);
            if (founds != null && founds.size() > 0) {
                Surat s = founds.get(0);
                List<Peminjam> list = peminjamDao.queryForEq(
                        Peminjam.UID_SURAT_FIELD_NAME, 
                        s.getUid()
                );
                boolean isDipinjam = false;
                for (Peminjam p : list) {
                    isDipinjam = p.getStatus().equals("dipinjam");
                    if (isDipinjam) {
                        s.setPeminjam(p);
                        break;
                    }
                }

                List<Attachment> attachments = attachmentDao.queryForEq(
                        Attachment.UID_SURAT_FIELD_NAME, 
                        s.getUid()
                );

                s.setIsDipinjam(isDipinjam);
                s.setAttachments(attachments);
                callback.onSuccess(s);
            }
            else callback.onError("Tidak terdapat arsip surat yang sesuai.");
        } catch (SQLException ex) {
            System.err.println("Error while searching Surat with uid ("+surat.getUid()+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public Surat findSuratByValue(String value) {
        if (suratList == null || suratList.isEmpty()) {
            try {
                loadSuratList();
            } catch (SQLException ex) {
                DialogBuilder.showError(GENERAL_ERROR_MESSAGE);
                return null;
            }
        }
        for (Surat s : suratList) {
            if (s.contains(value)) return s;
        }
        return null;
    }
    
    public Surat findSuratById(int uid) {
        for (Surat s : suratList) {
            if (s.getUid() == uid) return s;
        }
        return null;
    }
    
    public void setStatusSurat(int uid, boolean isDipinjam) {
        for (Surat s : suratList) {
            if (s.getUid() == uid) {
                s.setIsDipinjam(isDipinjam);
                break;
            }
        }
    }
    
    public void insertSurat(Surat surat, ICallback<String> callback) {
        try {
            suratDao.create(surat);
            suratList.add(surat);
            
            int uid = suratDao.extractId(surat);
            for (Attachment a : surat.getAttachments()) {
                a.setUidSurat(uid);
                addAttachment(a, callback);
            }
            
            callback.onSuccess("Surat berhasil disimpan.");
        } catch (SQLException ex) {
            System.err.println("Error while insert Surat with uid ("+surat.getUid()+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public void updateSurat(Surat surat, ICallback<String> callback) {
        try {
            suratDao.update(surat);
            callback.onSuccess("Surat berhasil dirubah.");
        } catch (SQLException ex) {
            System.err.println("Error while update Surat with uid ("+surat.getUid()+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public void deleteSurat(Surat surat, ICallback<String> callback) {
        try {
            suratDao.delete(surat);
            suratList.remove(surat);
            callback.onSuccess("Surat berhasil dihapus.");
        } catch (SQLException ex) {
            System.err.println("Error while delete Surat with uid ("+surat.getUid()+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public void addAttachment(Attachment attachment, ICallback<String> callback) {
        File file = new File(attachment.getPathFile());
        try {
            String path = FileUtils.copyFile(file);
            attachment.setPathFile(path);
            attachmentDao.create(attachment);
        } catch (IOException ex) {
            System.err.println("Error while copying File ("+attachment.getNamaFile()+"): "+ex);
            callback.onError(COPY_FILE_ERROR_MESSAGE);
        } catch (SQLException ex) {
            System.err.println("Error while insert Attachment with uid ("+attachment.getUid()+") : "+ex);
            callback.onError(ATTACHMENT_ERROR_MESSAGE);
        }
    }
    
    public void deleteAttachment(Attachment attachment, ICallback<String> callback) {
        try {
            attachmentDao.delete(attachment);
            callback.onSuccess("Attachment berhasil dihapus.");
        } catch (SQLException ex) {
            System.err.println("Error while delete Attachment with uid ("+attachment.getUid()+") : "+ex);
            callback.onError(ATTACHMENT_ERROR_MESSAGE);
        }
    }
    
    private void loadSuratList() throws SQLException {
        suratList.clear();
        for (Surat s : suratDao.queryForAll()) {
            List<Peminjam> list = peminjamDao.queryForEq(
                    Peminjam.UID_SURAT_FIELD_NAME, 
                    s.getUid()
            );
            boolean isDipinjam = false;
            for (Peminjam p : list) {
                isDipinjam = p.getStatus().equals("dipinjam");
                if (isDipinjam) {
                    s.setPeminjam(p);
                    break;
                }
            }
            
            List<Attachment> attachments = attachmentDao.queryForEq(
                    Attachment.UID_SURAT_FIELD_NAME, 
                    s.getUid()
            );
            
            s.setIsDipinjam(isDipinjam);
            s.setAttachments(attachments);
            suratList.add(s);
        }
    }
    
    private static final String GENERAL_ERROR_MESSAGE = 
            "Terjadi kesalahan ketika mengakses tabel Surat di database. Silahkan menghubungi admin.";
    private static final String ATTACHMENT_ERROR_MESSAGE = 
            "Terjadi kesalahan ketika mengakses tabel Attachment di database. Silahkan menghubungi admin.";
    private static final String COPY_FILE_ERROR_MESSAGE = 
            "Terjadi kesalahan ketika menyalin berkas attachment ke database. Silahkan menghubungi admin.";
}
