/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.manager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import id.krakatio.sonik.config.AppDatabase;
import id.krakatio.sonik.model.Biodata;
import id.krakatio.sonik.model.BiodataAnak;
import id.krakatio.sonik.util.DialogBuilder;
import id.krakatio.sonik.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Deri Armanda
 */
public class BiodataManager {
    
    private static BiodataManager SINGLETON;
    private final Dao<Biodata, Integer> biodataDao;
    private final Dao<BiodataAnak, Integer> biodataAnakDao;
    
    private BiodataManager() throws SQLException {
        biodataDao = DaoManager.createDao(
                AppDatabase.getConnection(), 
                Biodata.class
        );
        biodataAnakDao = DaoManager.createDao(
                AppDatabase.getConnection(), 
                BiodataAnak.class
        );
    }
    
    public static void init() {
        try {
            SINGLETON = new BiodataManager();
        } catch (SQLException ex) {
            SINGLETON = null;
            System.err.println("Error while creating Laporan DAO: "+ex);
            DialogBuilder.showError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public static BiodataManager getInstance() {
        if (SINGLETON == null) init();
        return SINGLETON;
    }
    
    public void insertBiodata(Biodata biodata, ICallback<String> callback) {
        try {
            if (biodata.getImagePath() != null) {
                File profile = new File(biodata.getImagePath());
                String path = FileUtils.copyFile(profile);
                biodata.setImagePath(path);
            }
            
            biodataDao.create(biodata);
            int uid = biodataDao.extractId(biodata);
            
            for (BiodataAnak anak : biodata.getListAnak()) {
                anak.setBiodataOrangTua(biodata);
                biodataAnakDao.create(anak);
            }
            
            callback.onSuccess("Data Biodata berhasil disimpan.");
        } catch (SQLException ex) {
            System.err.println("Error while insert Biodata with uid ("+biodata.getUid()+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        } catch (IOException ex) {
            System.err.println("Error while copying File : "+ex);
            callback.onError(COPY_FILE_ERROR_MESSAGE);
        }
    }
    
    public void updateBiodata(Biodata biodata, ICallback<String> callback) {
        try {
            File profile = new File(biodata.getImagePath());
            String path = FileUtils.copyFile(profile);
            biodata.setImagePath(path);
            
            biodataDao.update(biodata);
            callback.onSuccess("Data Biodata berhasil dirubah.");
        } catch (SQLException ex) {
            System.err.println("Error while update Biodata with uid ("+biodata.getUid()+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        } catch (IOException ex) {
            Logger.getLogger(BiodataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteBiodata(Biodata biodata, ICallback<String> callback) {
        try {
            biodataDao.delete(biodata);
            callback.onSuccess("Data Biodata berhasil dihapus.");
        } catch (SQLException ex) {
            System.err.println("Error while delete Biodata with uid ("+biodata.getUid()+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public void getBiodataList(ICallback<List<Biodata>> callback) {
        try {
            List<Biodata> list = biodataDao.queryForAll();
            for (Biodata b : list) {
                List<BiodataAnak> anak = biodataAnakDao.queryForEq(BiodataAnak.UID_ORANG_TUA_FIELD_NAME, b.getUid());
                b.setListAnak(anak);
            }            
            
            callback.onSuccess(list);
        } catch (SQLException ex) {
            System.err.println("Error while load all Biodata from Db: "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public void findBiodataByValue(String value, ICallback<Biodata> callback) {
        try {
            List<Biodata> list = biodataDao.queryForAll();
            for (Biodata b : list) {
                if (b.contains(value)) {
                    callback.onSuccess(b);
                    return;
                }
            }
        } catch (SQLException ex) {
            System.err.println("Error while load all Laporan from Db: "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    private static final String GENERAL_ERROR_MESSAGE = 
            "Terjadi kesalahan ketika mengakses tabel Formulir Biodata di database. Silahkan menghubungi admin.";
    private static final String COPY_FILE_ERROR_MESSAGE = 
            "Terjadi kesalahan ketika menyalin berkas Foto ke database. Silahkan menghubungi admin.";
}
