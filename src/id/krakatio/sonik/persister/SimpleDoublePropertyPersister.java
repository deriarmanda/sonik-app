/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.persister;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.BaseDataType;
import com.j256.ormlite.support.DatabaseResults;
import java.sql.SQLException;
import javafx.beans.property.SimpleDoubleProperty;

/**
 *
 * @author Deri Armanda
 */
public class SimpleDoublePropertyPersister extends BaseDataType {
    
    private static final SimpleDoublePropertyPersister SINGLETON = 
            new SimpleDoublePropertyPersister();

    private SimpleDoublePropertyPersister() {
        super(SqlType.DOUBLE, new Class<?>[]{Double.class});
    }

    /**
     * Parse a default String into a SimpleDoubleProperty.
     * @param fieldType Field Type
     * @param defaultStr Default string
     * @return SimpleDoubleProperty equal to value given by string
     */
    @Override
    public Object parseDefaultString(FieldType fieldType, String defaultStr) {
        return new SimpleDoubleProperty(Double.parseDouble(defaultStr));
    }

    /**
     * Getter for singleton of class.
     * @return
     */
    public static SimpleDoublePropertyPersister getSingleton() {
        return SINGLETON;
    }

    
    /**
     * Converts results into Sql argument
     * @param fieldType Field Type
     * @param results Results to convert
     * @param columnPos Column Position
     * @return SQL argument from input
     * @throws SQLException
     */
    @Override
    public Object resultToSqlArg(FieldType fieldType, DatabaseResults results, int columnPos) throws SQLException {
        return results.getDouble(columnPos);
    }

    /**
     * Converts SQL argument into a SimpleDoubleProperty.
     * @param fieldType Field Type
     * @param sqlArg SQL argument to convert
     * @param columnPos Column Position
     * @return SimpleDoubleProperty from SQL argument input
     */
    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) {
        return new SimpleDoubleProperty((Double) sqlArg);
    }

    /**
     * Converts a SimpleDoubleProperty into a double to be store in the database.
     * @param fieldType Field Type
     * @param javaObject SimpleDoubleProperty to convert
     * @return double equivalent to the input
     * @throws SQLException
     */
    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
        SimpleDoubleProperty DoubleProperty = (SimpleDoubleProperty) javaObject;
        return DoubleProperty.getValue();
    }

}
