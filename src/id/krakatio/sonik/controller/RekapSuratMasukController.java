/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.manager.ICallback;
import id.krakatio.sonik.manager.SuratManager;
import id.krakatio.sonik.model.Surat;
import id.krakatio.sonik.util.DialogBuilder;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class RekapSuratMasukController implements Initializable {

    @FXML
    private TableColumn<Surat, Number> no;
    @FXML
    private TableColumn<Surat, String> mk;
    @FXML
    private TableColumn<Surat, String> nomorSurat;
    @FXML
    private TableColumn<Surat, String> tanggalSurat;
    @FXML
    private TableColumn<Surat, String> kepada;
    @FXML
    private TableColumn<Surat, String> perihal;
    @FXML
    private TableColumn<Surat, String> kode;
    @FXML
    private TableView<Surat> rekapSuratMasuk;

    private SuratManager suratManager;
    private ObservableList<Surat> suratList;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        suratList = FXCollections.observableArrayList();
        suratManager = SuratManager.getInstance();
        
        no.setCellValueFactory(value -> new SimpleIntegerProperty(suratList.indexOf(value.getValue())+1));
        mk.setCellValueFactory(value -> value.getValue().jenisSuratProperty());
        nomorSurat.setCellValueFactory(value -> value.getValue().nomorSuratProperty());
        kode.setCellValueFactory(value -> value.getValue().kodeProperty());
        perihal.setCellValueFactory(value -> value.getValue().perihalProperty());
        tanggalSurat.setCellValueFactory(value -> value.getValue().tanggalSuratProperty());
        kepada.setCellValueFactory(value -> value.getValue().namaInstansiProperty());
        rekapSuratMasuk.setItems(suratList);
        
        suratManager.getSuratList(true, new ICallback<ObservableList<Surat>>() {
            @Override
            public void onSuccess(ObservableList<Surat> data) {
                suratList.clear();
                for (Surat s : data) {
                    if (s.getJenisSurat().equalsIgnoreCase("Masuk")) suratList.add(s);
                }
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
    }    

    @FXML
    private void btnPrint(MouseEvent event) {
    }
    
}
