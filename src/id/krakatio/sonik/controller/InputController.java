/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.manager.ICallback;
import id.krakatio.sonik.manager.SuratManager;
import id.krakatio.sonik.manager.UserManager;
import id.krakatio.sonik.model.Attachment;
import id.krakatio.sonik.model.Surat;
import id.krakatio.sonik.model.Unit;
import id.krakatio.sonik.util.DateTimeUtils;
import id.krakatio.sonik.util.DialogBuilder;
import id.krakatio.sonik.util.FileUtils;
import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class InputController implements Initializable {

    @FXML
    private TextField kepada;
    @FXML
    private TextField alamat;
    @FXML
    private TextField kota;
    @FXML
    private TextField nomorSurat;
    @FXML
    private DatePicker tanggalSurat;
    @FXML
    private TextField perihal;
    @FXML
    private DatePicker tanggalSimpan;
    @FXML
    private ComboBox<String> jenisSurat;
    @FXML
    private ComboBox<String> unit;
    @FXML
    private ComboBox<String> brsr;
    @FXML
    private ComboBox<String> sistemSImpan;
    @FXML
    private TextField indeks;
    @FXML
    private TextField nomorUrut;
    @FXML
    private TextField lampiran;
    @FXML
    private TextArea catatan;
    @FXML
    private TextField kodeSimpen;
    @FXML
    private TextArea isiRingkasan;
    @FXML
    private TextField arsiparis;
    @FXML
    private ListView<String> listAttachment;
    @FXML
    private Button btnTambah;
    @FXML
    private Button btnHapus;
    

    private SuratManager suratManager;
    private UserManager userManager;
    private Surat surat;
    private ArrayList<Attachment> listAtt;
    private ObservableList<String> attachments;
    
    @FXML
    private void ButtonTambah(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        
        File seletedFile = FileUtils.openFileDialog(stage);
        
        if(seletedFile != null) {            
            Attachment att = new Attachment();
            att.setNamaFile(seletedFile.getName());
            att.setPathFile(seletedFile.getAbsolutePath());
            listAtt.add(att);
                    
            attachments.add(att.getNamaFile());
        } else {
            System.out.println("File Tidak Valid");
        }
    }

    @FXML
    private void btnHapus(MouseEvent event) {
        String selected = listAttachment.getSelectionModel().getSelectedItem().toString();
        if (selected != null) {
            for (Attachment att : listAtt) {
                if (att.getNamaFile().equals(selected)) {
                    listAtt.remove(att);
                    attachments.remove(selected);
                    break;
                }
            }
        }
    }

    @FXML
    private void btnSimpanArsip(MouseEvent event) {
        //simpan ke database
        surat.setNamaInstansi(kepada.getText());
        surat.setAlamat(alamat.getText());
        surat.setKota(kota.getText());
        surat.setNomorSurat(nomorSurat.getText());
        surat.setPerihal(perihal.getText());
        surat.setTanggalSurat(DateTimeUtils.format(tanggalSurat.getValue()));
        surat.setTanggalSimpan(DateTimeUtils.format(tanggalSimpan.getValue()));
        surat.setJenisSurat(jenisSurat.getValue());
        surat.setUnit(unit.getValue());
        surat.setKerahasiaan(brsr.getValue());
        surat.setSistem(sistemSImpan.getValue());
        surat.setIndex(indeks.getText());
        surat.setKode(kodeSimpen.getText());
        surat.setNomorUrut(nomorUrut.getText());
        surat.setLampiran(lampiran.getText());
        surat.setRingkasan(isiRingkasan.getText());
        surat.setCatatan(catatan.getText());   
        surat.setPetugas(arsiparis.getText());
        surat.setAttachments(listAtt);
        
        suratManager.insertSurat(surat, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                DialogBuilder.showInfo(data);
                onSaveSuratSuccess();                
            }

            @Override
            public void onError(String message) {
                onSaveSuratError(message);
            }
            
        });
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        suratManager = SuratManager.getInstance();
        userManager = UserManager.getInstance();
        
        arsiparis.setText(userManager.getActiveUser().getFullName());
        
        tanggalSurat.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        tanggalSimpan.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        tanggalSimpan.setValue(LocalDate.now());
        
        jenisSurat.getItems().add("Masuk");
        jenisSurat.getItems().add("Keluar");
        jenisSurat.getSelectionModel().selectFirst();
        
        userManager.getUnit(new ICallback<List<Unit>>() {
            @Override
            public void onSuccess(List<Unit> data) {
                unit.getItems().clear();
                for (Unit u : data) {
                    unit.getItems().add(u.getNama());
                }
                unit.getSelectionModel().selectFirst();
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
        
        brsr.getItems().add("Biasa");
        brsr.getItems().add("Rahasia");
        brsr.getItems().add("Sangat Rahasia");
        brsr.getSelectionModel().selectFirst();
        
        sistemSImpan.getItems().add("Abjad");
        sistemSImpan.getItems().add("Tanggal Surat");
        sistemSImpan.getItems().add("Desimal");
        sistemSImpan.getItems().add("Terminal Digit");
        sistemSImpan.getItems().add("Wilayah");
        sistemSImpan.getSelectionModel().selectFirst();
        
        surat = new Surat();
        listAtt = new ArrayList();
        attachments = FXCollections.observableArrayList();
        listAttachment.setItems(attachments);
    }    
    
    private void onSaveSuratSuccess() {
        kepada.clear();
        alamat.clear();
        kota.clear();
        nomorSurat.clear();
        perihal.clear();
        tanggalSurat.setValue(null);
        tanggalSimpan.setValue(LocalDate.now());
        jenisSurat.getSelectionModel().selectFirst();
        unit.getSelectionModel().selectFirst();
        brsr.getSelectionModel().selectFirst();
        sistemSImpan.getSelectionModel().selectFirst();
        indeks.clear();
        kodeSimpen.clear();
        nomorUrut.clear();
        lampiran.clear();
        isiRingkasan.clear();
        catatan.clear();
        
        listAtt.clear();
        attachments.clear();
        surat = new Surat();
    }
    
    private void onSaveSuratError(String error) {
        DialogBuilder.showError(error);
    }
}
