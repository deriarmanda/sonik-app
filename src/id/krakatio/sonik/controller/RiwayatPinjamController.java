/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.manager.ICallback;
import id.krakatio.sonik.manager.PeminjamManager;
import id.krakatio.sonik.manager.SuratManager;
import id.krakatio.sonik.model.Peminjam;
import id.krakatio.sonik.model.Surat;
import id.krakatio.sonik.util.DialogBuilder;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class RiwayatPinjamController implements Initializable {

    @FXML
    private ComboBox<Surat> pilihData;
    @FXML
    private TableColumn<Peminjam, String> nama;
    @FXML
    private TableColumn<Peminjam, String> tanggalPinjam;
    @FXML
    private TableColumn<Peminjam, String> tanggalKembali;
    @FXML
    private TableColumn<Peminjam, String> petugas;
    @FXML
    private TableView<Peminjam> riwayatPeminjaman;
    
    private SuratManager suratManager;
    private PeminjamManager peminjamManager;
    private ObservableList<Surat> suratList;
    private Surat selectedSurat;
    private ObservableList<Peminjam> peminjamList;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        suratManager = SuratManager.getInstance();
        peminjamManager = PeminjamManager.getInstance();
        peminjamList = FXCollections.observableArrayList();
        suratList = FXCollections.observableArrayList();
        pilihData.setItems(suratList);
        
        nama.setCellValueFactory(value -> value.getValue().namaPeminjamProperty());
        tanggalPinjam.setCellValueFactory(value -> value.getValue().tanggalPinjamProperty());
        tanggalKembali.setCellValueFactory(value -> value.getValue().tanggalKembaliProperty());
        petugas.setCellValueFactory(value -> value.getValue().petugasProperty());
        riwayatPeminjaman.setItems(peminjamList);
        
        suratManager.getSuratList(true, new ICallback<ObservableList<Surat>>() {
            @Override
            public void onSuccess(ObservableList<Surat> data) {
                suratList.clear();
                suratList.addAll(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
        });
        
        pilihData.valueProperty().addListener((ObservableValue<? extends Surat> observable, Surat oldValue, Surat newValue) -> {
            selectedSurat = newValue;
            peminjamManager.getPeminjamListById(
                    selectedSurat.getUid(), 
                    new ICallback<List<Peminjam>>() {
                        @Override
                        public void onSuccess(List<Peminjam> data) {
                            peminjamList.clear();
                            peminjamList.addAll(data);
                        }

                        @Override
                        public void onError(String message) {
                            DialogBuilder.showError(message);
                        }
                    }
            );
        });
                
    }    
    
}
