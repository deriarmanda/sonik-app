/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.model.Laporan;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class DetailLaporanController implements Initializable {

    @FXML
    private TextField hari;
    @FXML
    private TextField tanggal;
    @FXML
    private TextField jam;
    @FXML
    private TextField tempat;
    @FXML
    private TextField acara;
    @FXML
    private TextArea pemimpinRapat;
    @FXML
    private TextArea pesertaRapat;
    @FXML
    private TextArea hasilRapat;

    /**
     * Initializes the controller class.
     */
    
    public void setLaporan(Laporan laporan) {
        hari.setText(laporan.getHari());
        tanggal.setText(laporan.getTanggal());
        jam.setText(laporan.getJam());
        tempat.setText(laporan.getTempat());
        acara.setText(laporan.getAcara());
        pemimpinRapat.setText(laporan.getPemimpin());
        pesertaRapat.setText(laporan.getPeserta());
        hasilRapat.setText(laporan.getHasil());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
