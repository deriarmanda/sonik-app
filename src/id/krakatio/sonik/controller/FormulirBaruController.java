/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.manager.BiodataManager;
import id.krakatio.sonik.manager.ICallback;
import id.krakatio.sonik.model.Biodata;
import id.krakatio.sonik.model.BiodataAnak;
import id.krakatio.sonik.util.DateTimeUtils;
import id.krakatio.sonik.util.DialogBuilder;
import id.krakatio.sonik.util.FileUtils;
import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class FormulirBaruController implements Initializable {

    @FXML
    private TextField namaLengkap;
    @FXML
    private TextField namaPanggilan;
    @FXML
    private ComboBox<String> jenisKelamin;
    @FXML
    private TextField tanggalMulaiKerja;
    @FXML
    private TextField jabatan;
    @FXML
    private TextField devisi;
    @FXML
    private ImageView image;
    @FXML
    private TextField nip;
    @FXML
    private TextField areaKerja;
    @FXML
    private TextArea alamatSesuaiKTP;
    @FXML
    private TextArea alamatSekarang;
    @FXML
    private TextField noTelpon;
    @FXML
    private TextField noHp;
    @FXML
    private TextField tempatLahir;
    @FXML
    private DatePicker tanggalLahir;
    @FXML
    private TextField Usia;
    @FXML
    private ComboBox<String> pendidikanTerakhir;
    @FXML
    private TextField namaSekolah;
    @FXML
    private TextField bank;
    @FXML
    private TextField noRekening;
    @FXML
    private TextField noKtp;
    @FXML
    private TextField noJamsostek;
    @FXML
    private TextField noNpwp;
    @FXML
    private ComboBox<String> statusSimC;
    @FXML
    private DatePicker berlakuSimC;
    @FXML
    private ComboBox<String> statusSimA;
    @FXML
    private DatePicker berlakuSimA;
    @FXML
    private ComboBox<String> statusSimB1;
    @FXML
    private DatePicker berlakuSimB1;
    @FXML
    private ComboBox<String> statusSimB1U;
    @FXML
    private DatePicker berlakuSimB1U;
    @FXML
    private ComboBox<String> statusSimB2U;
    @FXML
    private DatePicker berlakuSimB2U;
    @FXML
    private ComboBox<String> statusPerkawinan;
    @FXML
    private TextField namaSuami;
    @FXML
    private TextField tempatlahirKeluarga;
    @FXML
    private DatePicker tanggalLahirKeluarga;
    @FXML
    private DatePicker tanggalMenikah;
    @FXML
    private TextField usiaSuami;
    @FXML
    private TextField namaInput;
    @FXML
    private ComboBox<String> jkInput;
    @FXML
    private TextField usiaInput;
    @FXML
    private TextField pendidikanInput;
    @FXML
    private TextField pekerjaanInput;
    @FXML
    private TableView<BiodataAnak> tableData;
    @FXML
    private TableColumn<BiodataAnak, String> cNamaAnak;
    @FXML
    private TableColumn<BiodataAnak, String> cJenisKelamin;
    @FXML
    private TableColumn<BiodataAnak, String> cUsia;
    @FXML
    private TableColumn<BiodataAnak, String> cPendidikan;
    @FXML
    private TableColumn<BiodataAnak, String> cPekerjaan;

    private BiodataManager biodataManager;
    private File selectedPhoto;
    private ObservableList<BiodataAnak> listAnak;
    
    /**
     * Initializes the controller class.
     */    

    @FXML
    private void btnTambahData(MouseEvent event) {
        BiodataAnak anak = new BiodataAnak();
        anak.setNama(namaInput.getText());
        anak.setJenisKelamin(jkInput.getValue());
        anak.setUsia(usiaInput.getText());
        anak.setPendidikan(pendidikanInput.getText());
        anak.setPekerjaan(pekerjaanInput.getText());
        listAnak.add(anak);
    }

    @FXML
    private void btnBatal(MouseEvent event) {
        namaInput.clear();
        usiaInput.clear();
        pendidikanInput.clear();
        pekerjaanInput.clear();
    }

    @FXML
    private void btnSimpanFormulir(MouseEvent event) {
        Biodata data = new Biodata();
        data.setNamaLengkap(namaLengkap.getText());
        data.setNamaPanggilan(namaPanggilan.getText());
        data.setJenisKelamin(jenisKelamin.getValue());
        data.setTanggalMulaiKerja(tanggalMulaiKerja.getText());
        data.setJabatan(jabatan.getText());
        data.setDivisi(devisi.getText());
        data.setNip(nip.getText());
        data.setAreaKerja(areaKerja.getText());
        data.setAlamatKtp(alamatSesuaiKTP.getText());
        data.setAlamatSekarang(alamatSekarang.getText());
        data.setTeleponRumah(noTelpon.getText());
        data.setTeleponHp(noHp.getText());
        data.setTempatLahir(tempatLahir.getText());
        data.setTanggalLahir(DateTimeUtils.format(tanggalLahir.getValue()));
        data.setUsia(Usia.getText());
        data.setPendidikanTerakhir(pendidikanTerakhir.getValue());
        data.setSekolah(namaSekolah.getText());
        data.setBank(bank.getText());
        data.setNomorRekening(noRekening.getText());
        data.setNomorKtp(noKtp.getText());
        data.setJamsostek(noJamsostek.getText());
        data.setNpwp(noNpwp.getText());
        data.setSimCStatus(statusSimC.getValue());
        data.setSimCBerlaku(DateTimeUtils.format(berlakuSimC.getValue()));
        data.setSimAStatus(statusSimA.getValue());
        data.setSimABerlaku(DateTimeUtils.format(berlakuSimA.getValue()));
        data.setSimB1Status(statusSimB1.getValue());
        data.setSimB1Berlaku(DateTimeUtils.format(berlakuSimB1.getValue()));
        data.setSimB1UmumStatus(statusSimB1U.getValue());
        data.setSimB1UmumBerlaku(DateTimeUtils.format(berlakuSimB1U.getValue()));
        data.setSimB2Status(statusSimB2U.getValue());
        data.setSimB2Berlaku(DateTimeUtils.format(berlakuSimB2U.getValue()));
        data.setStatusPerkawinan(statusPerkawinan.getValue());
        data.setNamaIstri(namaSuami.getText());
        data.setTempatLahirIstri(tempatlahirKeluarga.getText());
        data.setTanggalLahirIstri(DateTimeUtils.format(tanggalLahirKeluarga.getValue()));
        data.setTanggalMenikah(DateTimeUtils.format(tanggalMenikah.getValue()));
        data.setUsiaIstri(usiaSuami.getText());
        
        if (selectedPhoto != null) data.setImagePath(selectedPhoto.getPath());
        data.setListAnak(listAnak);
        
        biodataManager.insertBiodata(data, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                clearForms();
                DialogBuilder.showInfo(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
    }

    @FXML
    private void btnUploadImage(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        
        selectedPhoto = FileUtils.openFileDialog(stage);
        if (selectedPhoto != null) {
            image.setImage(
                    new Image(selectedPhoto.toURI().toString(), 
                    image.getFitWidth(),
                    image.getFitHeight(),
                    false, false
            ));
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        biodataManager = BiodataManager.getInstance();
        listAnak = FXCollections.observableArrayList();
        
        cNamaAnak.setCellValueFactory(value -> value.getValue().namaProperty());
        cJenisKelamin.setCellValueFactory(value -> value.getValue().jenisKelaminProperty());
        cUsia.setCellValueFactory(value -> value.getValue().usiaProperty());
        cPendidikan.setCellValueFactory(value -> value.getValue().pendidikanProperty());
        cPekerjaan.setCellValueFactory(value -> value.getValue().pekerjaanProperty());
        tableData.setItems(listAnak);
        
        initFormDates();
        
        jenisKelamin.getItems().add("Laki - laki");
        jenisKelamin.getItems().add("Perempuan");
        jenisKelamin.getSelectionModel().selectFirst();
        
        pendidikanTerakhir.getItems().add("SD");
        pendidikanTerakhir.getItems().add("SMP");
        pendidikanTerakhir.getItems().add("SMA");
        pendidikanTerakhir.getItems().add("D1");
        pendidikanTerakhir.getItems().add("D3");
        pendidikanTerakhir.getItems().add("S1");
        pendidikanTerakhir.getItems().add("S2");
        pendidikanTerakhir.getSelectionModel().selectFirst();
        
        statusSimC.getItems().add("Tidak Ada");
        statusSimC.getItems().add("Ada");
        statusSimC.getSelectionModel().selectFirst();        
        
        statusSimA.getItems().add("Tidak Ada");
        statusSimA.getItems().add("Ada");
        statusSimA.getSelectionModel().selectFirst();        
        
        statusSimB1.getItems().add("Tidak Ada");
        statusSimB1.getItems().add("Ada");
        statusSimB1.getSelectionModel().selectFirst();        
        
        statusSimB1U.getItems().add("Tidak Ada");
        statusSimB1U.getItems().add("Ada");
        statusSimB1U.getSelectionModel().selectFirst();        
        
        statusSimB2U.getItems().add("Tidak Ada");
        statusSimB2U.getItems().add("Ada");
        statusSimB2U.getSelectionModel().selectFirst();  
        
        statusPerkawinan.getItems().add("Lajang");
        statusPerkawinan.getItems().add("Menikah");
        statusPerkawinan.getItems().add("Cerai Hidup");
        statusPerkawinan.getItems().add("Cerai Mati");
        statusPerkawinan.getSelectionModel().selectFirst();      
        
        jkInput.getItems().add("Laki - laki");
        jkInput.getItems().add("Perempuan");
        jkInput.getSelectionModel().selectFirst();      
    }
    
    private void initFormDates() {        
        tanggalLahir.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
                
        berlakuSimC.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
                
        berlakuSimA.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
                
        berlakuSimB1.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
                
        berlakuSimB1U.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
                
        berlakuSimB2U.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
                
        tanggalLahirKeluarga.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
                
        tanggalMenikah.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
    }
    
    private void clearForms() {
        image.setImage(null);
        selectedPhoto = null;
        listAnak.clear();
                
        namaLengkap.clear();
        namaPanggilan.clear();
        tanggalMulaiKerja.clear();
        jabatan.clear();
        devisi.clear();
        nip.clear();
        areaKerja.clear();
        alamatSesuaiKTP.clear();
        alamatSekarang.clear();
        noTelpon.clear();
        noHp.clear();
        tempatLahir.clear();
        tanggalLahir.setValue(null);
        Usia.clear();
        namaSekolah.clear();
        bank.clear();
        noRekening.clear();
        noKtp.clear();
        noJamsostek.clear();
        noNpwp.clear();
        berlakuSimC.setValue(null);
        berlakuSimA.setValue(null);
        berlakuSimB1.setValue(null);
        berlakuSimB1U.setValue(null);
        berlakuSimB2U.setValue(null);
        namaSuami.clear();
        tempatlahirKeluarga.clear();
        tanggalLahirKeluarga.setValue(null);
        tanggalMenikah.setValue(null);
        usiaSuami.clear();
    }
    
}
