/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.manager.ICallback;
import id.krakatio.sonik.manager.PeminjamManager;
import id.krakatio.sonik.model.Peminjam;
import id.krakatio.sonik.util.DialogBuilder;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class RekapPinjamArsipController implements Initializable {

    @FXML
    private TableView<Peminjam> rekapPinjamArsip;
    @FXML
    private TableColumn<Peminjam, Number> no;
    @FXML
    private TableColumn<Peminjam, String> idArsip;
    @FXML
    private TableColumn<Peminjam, String> namaPeminjam;
    @FXML
    private TableColumn<Peminjam, String> tanggalPinjam;
    @FXML
    private TableColumn<Peminjam, String> batasWaktu;
    @FXML
    private TableColumn<Peminjam, String> tanggalKembali;
    @FXML
    private TableColumn<Peminjam, String> petugas;

    private PeminjamManager peminjamManager;
    private ObservableList<Peminjam> peminjamList;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        peminjamManager = PeminjamManager.getInstance();
        peminjamList = FXCollections.observableArrayList();
        
        no.setCellValueFactory(value -> new SimpleIntegerProperty(peminjamList.indexOf(value.getValue())+1));
        idArsip.setCellValueFactory(value -> value.getValue().getSurat().namaInstansiProperty());
        namaPeminjam.setCellValueFactory(value -> value.getValue().namaPeminjamProperty());
        tanggalPinjam.setCellValueFactory(value -> value.getValue().tanggalPinjamProperty());
        batasWaktu.setCellValueFactory(value -> value.getValue().batasWaktuProperty());
        tanggalKembali.setCellValueFactory(value -> value.getValue().tanggalKembaliProperty());
        rekapPinjamArsip.setItems(peminjamList);
        
        peminjamManager.getPeminjamList(new ICallback<List<Peminjam>>() {
            @Override
            public void onSuccess(List<Peminjam> data) {
                peminjamList.clear();
                peminjamList.addAll(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
    }    
    
}
