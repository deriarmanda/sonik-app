/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.manager.ICallback;
import id.krakatio.sonik.manager.LaporanManager;
import id.krakatio.sonik.model.Laporan;
import id.krakatio.sonik.util.DateTimeUtils;
import id.krakatio.sonik.util.DialogBuilder;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class LaporanBaruController implements Initializable {

    @FXML
    private TextField hari;
    @FXML
    private DatePicker tanggal;
    @FXML
    private TextField jam;
    @FXML
    private TextField tempat;
    @FXML
    private TextField acara;
    @FXML
    private TextArea pemimpinRapat;
    @FXML
    private TextArea pesertaRapat;
    @FXML
    private TextArea hasilRapat;
    
    private LaporanManager laporanManager;
    
    @FXML
    private void btnSimpanHasilRapat(MouseEvent event) {
        Laporan laporan = new Laporan();
        laporan.setHari(hari.getText());
        laporan.setTanggal(DateTimeUtils.format(tanggal.getValue()));
        laporan.setJam(jam.getText());
        laporan.setTempat(tempat.getText());
        laporan.setAcara(acara.getText());
        laporan.setPemimpin(pemimpinRapat.getText());
        laporan.setPeserta(pesertaRapat.getText());
        laporan.setHasil(hasilRapat.getText());
        
        laporanManager.insertLaporan(laporan, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                DialogBuilder.showInfo(data);
                clearForm();
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        laporanManager = LaporanManager.getInstance();
        tanggal.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        tanggal.setValue(LocalDate.now());
    }
    
    private void clearForm() {
        hari.clear();
        tanggal.setValue(LocalDate.now());
        jam.clear();
        tempat.clear();
        acara.clear();
        pemimpinRapat.clear();
        pesertaRapat.clear();
        hasilRapat.clear();
    }
}
