/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.model.Biodata;
import id.krakatio.sonik.model.Surat;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class DetailFormulirController implements Initializable {

    @FXML
    private TextField namaLengkap;
    @FXML
    private TextField namaPanggilan;
    @FXML
    private TextField jenisKelamin;
    @FXML
    private TextField tanggalMulaiKerja;
    @FXML
    private TextField jabatan;
    @FXML
    private TextField devisi;
    @FXML
    private ImageView image;
    @FXML
    private TextField nip;
    @FXML
    private TextField areaKerja;
    @FXML
    private TextArea alamatSesuaiKTP;
    @FXML
    private TextArea alamatSekarang;
    @FXML
    private TextField noTelpon;
    @FXML
    private TextField noHp;
    @FXML
    private TextField tempatLahir;
    @FXML
    private TextField tanggalLahir;
    @FXML
    private TextField Usia;
    @FXML
    private TextField pendidikanTerakhir;
    @FXML
    private TextField namaSekolah;
    @FXML
    private TextField bank;
    @FXML
    private TextField noRekening;
    @FXML
    private TextField noKtp;
    @FXML
    private TextField masaBerlakukKtp;
    @FXML
    private TextField noJamsostek;
    @FXML
    private TextField noNpwp;
    @FXML
    private TextField statusSimC;
    @FXML
    private TextField berlakuSimC;
    @FXML
    private TextField statusSimA;
    @FXML
    private TextField berlakuSimA;
    @FXML
    private TextField statusSimB1;
    @FXML
    private TextField berlakuSimB1;
    @FXML
    private TextField statusSimB1U;
    @FXML
    private TextField berlakuSimB1U;
    @FXML
    private TextField statusSimB2U;
    @FXML
    private TextField berlakuSimB2U;
    @FXML
    private TextField statusPerkawinan;
    @FXML
    private TextField namaSuami;
    @FXML
    private TextField tempatlahirKeluarga;
    @FXML
    private TextField tanggalLahirKeluarga;
    @FXML
    private TextField tanggalMenikah;
    @FXML
    private TextField usiaSuami;
    @FXML
    private TextField namaInput;
    @FXML
    private TextField jkInput;
    @FXML
    private TextField usiaInput;
    @FXML
    private TextField pendidikanInput;
    @FXML
    private TextField pekerjaanInput;
    @FXML
    private TableView<?> tableData;
    @FXML
    private TableColumn<?, ?> cNamaAnak;
    @FXML
    private TableColumn<?, ?> cJenisKelamin;
    @FXML
    private TableColumn<?, ?> cUsia;
    @FXML
    private TableColumn<?, ?> cPendidikan;
    @FXML
    private TableColumn<?, ?> cPekerjaan;

    /**
     * Initializes the controller class.
     */
    
    private Biodata biodata;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        namaLengkap.setText(biodata.getNamaLengkap());
        namaPanggilan.setText(biodata.getNamaPanggilan());
        jenisKelamin.setText(biodata.getJenisKelamin());
        tanggalMulaiKerja.setText(biodata.getTanggalMulaiKerja());
        jabatan.setText(biodata.getJabatan());
        devisi.setText(biodata.getDivisi());
        image.setImage(biodata.getImagePath());
        nip.setText(biodata.getNip());
        areaKerja.setText(biodata.getAreaKerja());
        alamatSesuaiKTP.setText(biodata.getAlamatKtp());
        alamatSekarang.setText(biodata.getAlamatSekarang());
        noTelpon.setText(biodata.getTeleponRumah());
        noHp.setText(biodata.getTeleponHp());
        tempatLahir.setText(biodata.getTempatLahir());
        tanggalLahir.setText(biodata.getTanggalLahir());
        Usia.setText(biodata.getUsia());
        pendidikanTerakhir.setText(biodata.getPendidikanTerakhir());
        namaSekolah.setText(biodata.getSekolah());
        bank.setText(biodata.getBank());
        noRekening.setText(biodata.getNomorRekening());
        noKtp.setText(biodata.getNomorKtp());
        noJamsostek.setText(biodata.getJamsostek());
        noNpwp.setText(biodata.getNpwp());
        statusSimC.setText(biodata.getSimCStatus());
        berlakuSimC.setText(biodata.getSimCBerlaku());
        statusSimA.setText(biodata.getSimAStatus());
        berlakuSimA.setText(biodata.getSimABerlaku());
        statusSimB1.setText(biodata.getSimB1Status());
        berlakuSimB1.setText(biodata.getSimB1Berlaku());
        statusSimB1U.setText(biodata.getSimB1UmumStatus());
        berlakuSimB1U.setText(biodata.getSimB1UmumBerlaku());
        statusSimB2U.setText(biodata.getSimB2Status());
        berlakuSimB2U.setText(biodata.getSimB2Berlaku());
        statusPerkawinan.setText(biodata.getStatusPerkawinan());
        namaSuami.setText(biodata.getNamaIstri());
        tempatlahirKeluarga.setText(biodata.getTempatLahirIstri());
        tanggalLahirKeluarga.setText(biodata.getTanggalLahirIstri());
        tanggalMenikah.setText(biodata.getTanggalMenikah());
        usiaSuami.setText(biodata.getUsiaIstri());
        namaInput.setText(biodata.getIndex());
        jkInput.setText(biodata.getIndex());
        usiaInput.setText(biodata.getIndex());
        pendidikanInput.setText(biodata.getIndex());
        pekerjaanInput.setText(biodata.getIndex());
    }    


    
}
