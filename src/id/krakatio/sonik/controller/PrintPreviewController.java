/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class PrintPreviewController implements Initializable {

    @FXML
    private Pane panePrint;
    @FXML
    private VBox boxPrint;
    @FXML
    private ImageView logo;
    @FXML
    private Text header1;
    @FXML
    private Text header2;
    @FXML
    private Text header3;
    @FXML
    private Text header4;
    @FXML
    private Text kontak;
    @FXML
    private TableView<?> rekapSuratMasuk;
    @FXML
    private TableColumn<?, ?> no;
    @FXML
    private TableColumn<?, ?> mk;
    @FXML
    private TableColumn<?, ?> nomorSurat;
    @FXML
    private TableColumn<?, ?> tanggalSurat;
    @FXML
    private TableColumn<?, ?> kepada;
    @FXML
    private TableColumn<?, ?> perihal;
    @FXML
    private TableColumn<?, ?> kode;

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
}
