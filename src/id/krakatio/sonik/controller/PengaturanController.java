/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.manager.ICallback;
import id.krakatio.sonik.manager.UserManager;
import id.krakatio.sonik.model.Unit;
import id.krakatio.sonik.model.User;
import id.krakatio.sonik.util.DialogBuilder;
import id.krakatio.sonik.util.FileUtils;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class PengaturanController implements Initializable {

    @FXML
    private ComboBox<Integer> pilihId;
    @FXML
    private TextField userName;
    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField email;
    @FXML
    private TextField password;
    @FXML
    private ImageView imageProfile;
    @FXML
    private TableView<Unit> tableUnit;
    @FXML
    private TableColumn<Unit, String> cNamaUnit;

    private UserManager userManager;
    private List<User> userList;
    private User selectedUser;
    private File choosenUserPhoto;
    private ObservableList<Unit> unitList;
    private Unit selectedUnit;
    @FXML
    private TextField unit;
    @FXML
    private ImageView logo;
    @FXML
    private TextField header1;
    @FXML
    private TextField header2;
    @FXML
    private TextField header3;
    @FXML
    private TextField header4;
    @FXML
    private TextArea kontak;
    
    /**
     * Initializes the controller class.
     */    

    @FXML
    private void btnUploadProfile(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        
        choosenUserPhoto = FileUtils.openFileDialog(stage);
        if (choosenUserPhoto != null) {
            imageProfile.setImage(
                    new Image(choosenUserPhoto.toURI().toString(), 
                    imageProfile.getFitWidth(),
                    imageProfile.getFitHeight(),
                    false, false
            ));
        }
    }

    @FXML
    private void btnSimpanAccount(MouseEvent event) {
        User user = new User();
        user.setUid(-1);
        user.setUsername(userName.getText());
        user.setFirstName(firstName.getText());
        user.setLastName(lastName.getText());
        user.setEmail(email.getText());
        user.setPassword(password.getText());
        user.setImagePath(choosenUserPhoto.getPath());
        
        userManager.createUser(user, new ICallback<User>() {
            @Override
            public void onSuccess(User data) {
                userList.add(data);
                pilihId.getItems().add(data.getUid());
                pilihId.getSelectionModel().selectLast();
                DialogBuilder.showInfo("Berhasil membuat akun baru.");
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }        
        });
    }

    @FXML
    private void btnPernaharuiAccount(MouseEvent event) {
        selectedUser.setUsername(userName.getText());
        selectedUser.setFirstName(firstName.getText());
        selectedUser.setLastName(lastName.getText());
        selectedUser.setEmail(email.getText());
        selectedUser.setPassword(password.getText());
        selectedUser.setImagePath(choosenUserPhoto.getPath());
        
        userManager.updateUser(selectedUser, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                DialogBuilder.showInfo(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }        
        });
    }

    @FXML
    private void btnHapusAccount(MouseEvent event) {
        userManager.deleteUser(selectedUser, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                userList.remove(selectedUser);
                pilihId.getItems().remove(Integer.valueOf(selectedUser.getUid()));
                DialogBuilder.showInfo(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }        
        });
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        userManager = UserManager.getInstance();
        selectedUser = new User();
        unitList = FXCollections.observableArrayList();
        
        cNamaUnit.setCellValueFactory(value -> value.getValue().namaProperty());
        tableUnit.setItems(unitList);
        tableUnit.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Unit> observable, 
                        Unit oldValue, 
                        Unit newValue) -> {
            selectedUnit = newValue;          
            unit.setText(selectedUnit.getNama());
        });
        
        userManager.getAvailableUser(new ICallback<List<User>>() {
            @Override
            public void onSuccess(List<User> data) {
                userList = data;
                for (User user : userList) {
                    pilihId.getItems().add(user.getUid());
                }
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
        userManager.getUnit(new ICallback<List<Unit>>() {
            @Override
            public void onSuccess(List<Unit> data) {
                unitList.clear();
                unitList.addAll(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
        
        pilihId.valueProperty().addListener((ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) -> {
            for (User user : userList) {
                if (user.getUid() == newValue) {
                    selectedUser = user;
                    fetchUser(user);
                    break;
                }
            }
        });
    }

    @FXML
    private void btnTambahUnit(MouseEvent event) {
        Unit unitBaru = new Unit();
        unitBaru.setNama(unit.getText());
        
        userManager.insertUnit(unitBaru, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                unitList.add(unitBaru);
                unit.clear();
                selectedUnit = null;
                DialogBuilder.showInfo(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
    }

    @FXML
    private void btnHapusUnit(MouseEvent event) {
        if (selectedUnit != null) {
            userManager.deleteUnit(selectedUnit, new ICallback<String>() {
                @Override
                public void onSuccess(String data) {
                    unitList.remove(selectedUnit);
                    unit.clear();
                    selectedUnit = null;
                    DialogBuilder.showInfo(data);
                }

                @Override
                public void onError(String message) {
                    DialogBuilder.showError(message);
                }
                
            });
        }
    }
    
    private void fetchUser(User data) {
        userName.setText(data.getUsername());
        firstName.setText(data.getFirstName());
        lastName.setText(data.getLastName());
        email.setText(data.getEmail());
        password.setText(data.getPassword());
        
        if (data.getImagePath() != null) {
            File profile = new File(data.getImagePath());
            imageProfile.setImage(
                    new Image(profile.toURI().toString(), 
                    imageProfile.getFitWidth(),
                    imageProfile.getFitHeight(),
                    false, false
            ));
        }       
    }

    @FXML
    private void chooseFileLogo(MouseEvent event) {
    }

    @FXML
    private void gantiHeader(MouseEvent event) {
    }
}
