/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.manager.BiodataManager;
import id.krakatio.sonik.manager.ICallback;
import id.krakatio.sonik.model.Biodata;
import id.krakatio.sonik.util.DialogBuilder;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class RekapFormulirController implements Initializable {

    @FXML
    private TextField searchRekapFormulir;
    @FXML
    private TableView<Biodata> rekapFormulir;
    @FXML
    private TableColumn<Biodata, String> namaLengkap;
    @FXML
    private TableColumn<Biodata, String> namaPanggilan;
    @FXML
    private TableColumn<Biodata, String> jenisKelamin;
    @FXML
    private TableColumn<Biodata, String> tanggalMulaiKerja;
    @FXML
    private TableColumn<Biodata, String> jabatan;
    @FXML
    private TableColumn<Biodata, String> devisi;
    @FXML
    private TableColumn<Biodata, String> nip;
    @FXML
    private TableColumn<Biodata, String> areaKerja;
    @FXML
    private TableColumn<Biodata, String> alamatKTP;
    @FXML
    private TableColumn<Biodata, String> alamatSekarang;
    @FXML
    private TableColumn<Biodata, String> noTelp;
    @FXML
    private TableColumn<Biodata, String> noHp;
    @FXML
    private TableColumn<Biodata, String> tempatLahir;
    @FXML
    private TableColumn<Biodata, String> tanggalLahir;
    @FXML
    private TableColumn<Biodata, String> usia;
    @FXML
    private TableColumn<Biodata, String> pendTerakhir;
    @FXML
    private TableColumn<Biodata, String> namaSekolah;
    @FXML
    private TableColumn<Biodata, String> bank;
    @FXML
    private TableColumn<Biodata, String> noRekening;
    @FXML
    private TableColumn<Biodata, String> noKtp;
    @FXML
    private TableColumn<Biodata, String> noKartuJamsostek;
    @FXML
    private TableColumn<Biodata, String> noNpwp;

    private BiodataManager biodataManager;
    private List<Biodata> biodataList;
    private ObservableList<Biodata> tableData;
    private Biodata selectedBiodata;
    
    /**
     * Initializes the controller class.
     */    

    @FXML
    private void detailFormulir(MouseEvent event) {
        if (selectedBiodata != null) {
            //
        }
    }
    
    @FXML
    private void actionSearchFormulir(ActionEvent event) {
        String query = searchRekapFormulir.getText();
        tableData.clear();
        if (query.trim().isEmpty()) {
            tableData.addAll(biodataList);
        } else {
            for (Biodata b : biodataList) {
                if (b.contains(query)) tableData.add(b);
            }
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        biodataManager = BiodataManager.getInstance();
        tableData = FXCollections.observableArrayList();
                
        namaLengkap.setCellValueFactory(value -> value.getValue().namaLengkapProperty());
        namaPanggilan.setCellValueFactory(value -> value.getValue().namaPanggilanProperty());
        jenisKelamin.setCellValueFactory(value -> value.getValue().jenisKelaminProperty());
        tanggalMulaiKerja.setCellValueFactory(value -> value.getValue().tanggalMulaiKerjaProperty());
        jabatan.setCellValueFactory(value -> value.getValue().jabatanProperty());
        devisi.setCellValueFactory(value -> value.getValue().divisiProperty());
        nip.setCellValueFactory(value -> value.getValue().nipProperty());
        areaKerja.setCellValueFactory(value -> value.getValue().areaKerjaProperty());
        alamatKTP.setCellValueFactory(value -> value.getValue().alamatKtpProperty());
        alamatSekarang.setCellValueFactory(value -> value.getValue().alamatSekarangProperty());
        noTelp.setCellValueFactory(value -> value.getValue().teleponRumahProperty());
        noHp.setCellValueFactory(value -> value.getValue().teleponHpProperty());
        tempatLahir.setCellValueFactory(value -> value.getValue().tempatLahirProperty());
        tanggalLahir.setCellValueFactory(value -> value.getValue().tanggalLahirProperty());
        usia.setCellValueFactory(value -> value.getValue().usiaProperty());
        pendTerakhir.setCellValueFactory(value -> value.getValue().pendidikanTerakhirProperty());
        namaSekolah.setCellValueFactory(value -> value.getValue().sekolahProperty());
        bank.setCellValueFactory(value -> value.getValue().bankProperty());
        noRekening.setCellValueFactory(value -> value.getValue().nomorKtpProperty());
        noKtp.setCellValueFactory(value -> value.getValue().nomorKtpProperty());
        noKartuJamsostek.setCellValueFactory(value -> value.getValue().jamsostekProperty());
        noNpwp.setCellValueFactory(value -> value.getValue().npwpProperty());
        
        rekapFormulir.setItems(tableData);
        rekapFormulir.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Biodata> observable, 
                        Biodata oldValue, 
                        Biodata newValue) -> {
            selectedBiodata = newValue;            
        });
        
        biodataManager.getBiodataList(new ICallback<List<Biodata>>() {
            @Override
            public void onSuccess(List<Biodata> data) {
                biodataList = data;
                tableData.clear();
                tableData.addAll(biodataList);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }            
        });
    }
}
