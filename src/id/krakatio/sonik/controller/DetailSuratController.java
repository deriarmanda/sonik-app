/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.manager.ICallback;
import id.krakatio.sonik.manager.SuratManager;
import id.krakatio.sonik.manager.UserManager;
import id.krakatio.sonik.model.Attachment;
import id.krakatio.sonik.model.Surat;
import id.krakatio.sonik.model.Unit;
import id.krakatio.sonik.util.DateTimeUtils;
import id.krakatio.sonik.util.DialogBuilder;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class DetailSuratController implements Initializable {

    @FXML
    private ComboBox<Surat> pilihData;
    @FXML
    private TextField kepada;
    @FXML
    private TextField alamat;
    @FXML
    private TextField kota;
    @FXML
    private TextField nomorSurat;
    @FXML
    private DatePicker tanggalSurat;
    @FXML
    private TextField perihal;
    @FXML
    private DatePicker tanggalSimpan;
    @FXML
    private ComboBox<String> jenisSurat;
    @FXML
    private ComboBox<String> unit;
    @FXML
    private ComboBox<String> brsr;
    @FXML
    private ComboBox<String> sistemSImpan;
    @FXML
    private TextField indeks;
    @FXML
    private TextField nomorUrut;
    @FXML
    private TextField lampiran;
    @FXML
    private TextArea catatan;
    @FXML
    private TextField kodeSimpen;
    @FXML
    private TextArea isiRingkasan;
    @FXML
    private TextField arsiparis;
    @FXML
    private ListView<String> listAttachment;
    @FXML
    private Text statusSurat;

    private SuratManager suratManager;
    private UserManager userManager;
    private ObservableList<Surat> suratList;
    private Surat selectedSurat;
    private ArrayList<Attachment> attachments;
    private ObservableList<String> listAtt;
    
    /**
     * Initializes the controller class.
     */
        
    @FXML
    private void btnSimpanArsip(MouseEvent event) {
        selectedSurat.setNamaInstansi(kepada.getText());
        selectedSurat.setAlamat(alamat.getText());
        selectedSurat.setKota(kota.getText());
        selectedSurat.setNomorSurat(nomorSurat.getText());
        selectedSurat.setPerihal(perihal.getText());
        selectedSurat.setTanggalSurat(DateTimeUtils.format(tanggalSurat.getValue()));
        selectedSurat.setTanggalSimpan(DateTimeUtils.format(tanggalSimpan.getValue()));
        selectedSurat.setJenisSurat(jenisSurat.getValue());
        selectedSurat.setUnit(unit.getValue());
        selectedSurat.setKerahasiaan(brsr.getValue());
        selectedSurat.setSistem(sistemSImpan.getValue());
        selectedSurat.setIndex(indeks.getText());
        selectedSurat.setKode(kodeSimpen.getText());
        selectedSurat.setNomorUrut(nomorUrut.getText());
        selectedSurat.setLampiran(lampiran.getText());
        selectedSurat.setRingkasan(isiRingkasan.getText());
        selectedSurat.setCatatan(catatan.getText());       
        selectedSurat.setPetugas(arsiparis.getText());
        
        suratManager.updateSurat(selectedSurat, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                DialogBuilder.showInfo(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }            
        });
    }

    @FXML
    private void ButtonTambah(ActionEvent event) {
    }

    @FXML
    private void btnHapus(MouseEvent event) {
    }

    @FXML
    private void btnHapusArsip(MouseEvent event) {
        suratManager.deleteSurat(selectedSurat, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                suratList.remove(selectedSurat);
                pilihData.getSelectionModel().selectFirst();
                DialogBuilder.showInfo(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }            
        });
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        suratManager = SuratManager.getInstance();    
        userManager = UserManager.getInstance();
        
        selectedSurat = new Surat();
        attachments = new ArrayList();
        suratList = FXCollections.observableArrayList();
        pilihData.setItems(suratList);
        listAtt = FXCollections.observableArrayList();
        listAttachment.setItems(listAtt);
        
        suratManager.getSuratList(true, new ICallback<ObservableList<Surat>>() {
            @Override
            public void onSuccess(ObservableList<Surat> data) {
                suratList.clear();
                suratList.addAll(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
        });
        
        tanggalSurat.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        tanggalSimpan.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        tanggalSimpan.setValue(LocalDate.now());
        
        jenisSurat.getItems().add("Masuk");
        jenisSurat.getItems().add("Keluar");
        jenisSurat.getSelectionModel().selectFirst();
        
        userManager.getUnit(new ICallback<List<Unit>>() {
            @Override
            public void onSuccess(List<Unit> data) {
                unit.getItems().clear();
                for (Unit u : data) {
                    unit.getItems().add(u.getNama());
                }
                unit.getSelectionModel().selectFirst();
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
        
        brsr.getItems().add("Biasa");
        brsr.getItems().add("Rahasia");
        brsr.getItems().add("Sangat Rahasia");
        brsr.getSelectionModel().selectFirst();
        
        sistemSImpan.getItems().add("Abjad");
        sistemSImpan.getItems().add("Tanggal Surat");
        sistemSImpan.getItems().add("Desimal");
        sistemSImpan.getItems().add("Terminal Digit");
        sistemSImpan.getItems().add("Wilayah");
        sistemSImpan.getSelectionModel().selectFirst();
        
        pilihData.valueProperty().addListener((ObservableValue<? extends Surat> observable, Surat oldValue, Surat newValue) -> {
            selectedSurat = newValue;
            fetchSurat();
        });
        
        arsiparis.setText(userManager.getActiveUser().getFullName());
    }    
    
    public void setSelectedSurat(Surat surat) {
        selectedSurat = surat;
        fetchSurat();
    }
    
    private void fetchSurat() {
        // Data surat
        kepada.setText(selectedSurat.getNamaInstansi());
        alamat.setText(selectedSurat.getAlamat());
        kota.setText(selectedSurat.getKota());
        nomorSurat.setText(selectedSurat.getNomorSurat());
        perihal.setText(selectedSurat.getPerihal());
        jenisSurat.getSelectionModel().select(selectedSurat.getJenisSurat());
        unit.getSelectionModel().select(selectedSurat.getUnit());
        tanggalSurat.setValue(DateTimeUtils.parseDateOnly(selectedSurat.getTanggalSurat()));
        tanggalSimpan.setValue(DateTimeUtils.parseDateOnly(selectedSurat.getTanggalSimpan()));
        brsr.getSelectionModel().select(selectedSurat.getKerahasiaan());
        sistemSImpan.getSelectionModel().select(selectedSurat.getSistem());
        indeks.setText(selectedSurat.getIndex());
        kodeSimpen.setText(selectedSurat.getKode());
        nomorUrut.setText(selectedSurat.getNomorUrut());
        lampiran.setText(selectedSurat.getLampiran());
        isiRingkasan.setText(selectedSurat.getRingkasan());
        catatan.setText(selectedSurat.getCatatan());
        
        attachments.clear();
        attachments.addAll(selectedSurat.getAttachments());
        listAtt.clear();
        attachments.forEach((a) -> { listAtt.add(a.getNamaFile()); });
        
        statusSurat.setVisible(selectedSurat.isDipinjam());
    }
}
