/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.manager.ICallback;
import id.krakatio.sonik.manager.PeminjamManager;
import id.krakatio.sonik.manager.SuratManager;
import id.krakatio.sonik.manager.UserManager;
import id.krakatio.sonik.model.Peminjam;
import id.krakatio.sonik.model.Surat;
import id.krakatio.sonik.util.DateTimeUtils;
import id.krakatio.sonik.util.DialogBuilder;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class InfoPeminjamController implements Initializable {

    @FXML
    private ComboBox<Surat> pilihData;
    @FXML
    private TextField namaPeminjam;
    @FXML
    private TextField statusArsip;
    @FXML
    private TextField jabatan;
    @FXML
    private TextField petugas;
    @FXML
    private DatePicker tanggalPinjam;
    @FXML
    private DatePicker tanggalKembali;
    @FXML
    private VBox formPengembalian;
    @FXML
    private DatePicker tanggalPengambalian;
    @FXML
    private TextField petugasPengembalian;
    @FXML
    private HBox simpanPengembalian;
    @FXML
    private HBox hapusPengembalian;  
    @FXML
    private HBox buttonPinjam;
    @FXML
    private HBox buttonKembali;
    
    private UserManager userManager;
    private SuratManager suratManager;
    private PeminjamManager peminjamManager;
    private ObservableList<Surat> suratList;
    private Surat selectedSurat;
    
    /**
     * Initializes the controller class.
     */    

    @FXML
    private void btnSimpanArsip(MouseEvent event) {
        Peminjam peminjam = new Peminjam();
        peminjam.setNamaPeminjam(namaPeminjam.getText());
        peminjam.setJabatan(jabatan.getText());
        peminjam.setPetugas(petugas.getText());
        peminjam.setStatus("dipinjam");
        peminjam.setSurat(selectedSurat);
        peminjam.setTanggalKembali(DateTimeUtils.format(tanggalKembali.getValue()));
        peminjam.setTanggalPinjam(DateTimeUtils.format(tanggalPinjam.getValue()));
        
        peminjamManager.insertPeminjam(peminjam, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                namaPeminjam.clear();
                jabatan.clear();
                tanggalKembali.setValue(null);
                DialogBuilder.showInfo(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
    }

    @FXML
    private void btnPengembalianArsip(MouseEvent event) {
        tanggalPengambalian.setValue(LocalDate.now());
        petugasPengembalian.setText(userManager.getActiveUser().getFullName());
        formPengembalian.setVisible(true);
    }

    @FXML
    private void simpanPengembalian(MouseEvent event) {
        Peminjam selectedPeminjam = selectedSurat.getPeminjam();
        selectedPeminjam.setStatus("kembali");
        selectedPeminjam.setPetugas(petugasPengembalian.getText());
        selectedPeminjam.setTanggalKembali(DateTimeUtils.format(tanggalPengambalian.getValue()));
        
        peminjamManager.updatePeminjam(selectedPeminjam, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                namaPeminjam.clear();
                statusArsip.setText("Tersedia");
                jabatan.clear();
                petugas.setText(userManager.getActiveUser().getFullName());
                tanggalPinjam.setValue(null);
                tanggalKembali.setValue(null);
                suratManager.setStatusSurat(selectedSurat.getUid(), false);
                DialogBuilder.showInfo("Data pengembalian berhasil disimpan.");
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }            
        });
    }

    @FXML
    private void hapusPengembalian(MouseEvent event) {
        formPengembalian.setVisible(true);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO        
        userManager = UserManager.getInstance();
        suratManager = SuratManager.getInstance();
        peminjamManager = PeminjamManager.getInstance();
        suratList = FXCollections.observableArrayList();
        pilihData.setItems(suratList);
        
        suratManager.getSuratList(true, new ICallback<ObservableList<Surat>>() {
            @Override
            public void onSuccess(ObservableList<Surat> data) {
                suratList.clear();
                suratList.addAll(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
        });
        
        pilihData.valueProperty().addListener((ObservableValue<? extends Surat> observable, Surat oldValue, Surat newValue) -> {
            selectedSurat = newValue;    
            fetchPeminjam();
        });
    }
    
    private void fetchPeminjam() {
        Peminjam p = selectedSurat.getPeminjam();
        if (selectedSurat.isDipinjam()) {
            buttonPinjam.setVisible(false);
            buttonKembali.setVisible(true);
            namaPeminjam.setText(p.getNamaPeminjam());
            statusArsip.setText(p.getStatus());
            jabatan.setText(p.getJabatan());
            petugas.setText(p.getPetugas());
            tanggalPinjam.setValue(DateTimeUtils.parseDateOnly(p.getTanggalPinjam()));
            tanggalKembali.setValue(DateTimeUtils.parseDateOnly(p.getTanggalKembali()));
        } else {
            buttonPinjam.setVisible(true);
            buttonKembali.setVisible(false);
            namaPeminjam.clear();
            statusArsip.setText("Tersedia");
            jabatan.clear();
            petugas.setText(userManager.getActiveUser().getFullName());
            tanggalPinjam.setValue(null);
            tanggalKembali.setValue(null);
        }
    }
}
