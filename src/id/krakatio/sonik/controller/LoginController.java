/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.manager.ICallback;
import id.krakatio.sonik.manager.UserManager;
import id.krakatio.sonik.model.User;
import id.krakatio.sonik.util.DialogBuilder;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Deri Armanda
 */
public class LoginController implements Initializable {

    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    
    private UserManager userManager;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        userManager = UserManager.getInstance();
    }    

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */    

    @FXML
    private void btnLogin(MouseEvent event)throws IOException {
//        onLoginSuccess((Node) event.getSource());
        User user = new User();
        user.setUsername(username.getText());
        user.setPassword(password.getText());
        
        userManager.authenticate(user, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                DialogBuilder.showInfo(data);
                onLoginSuccess((Node) event.getSource());
            }

            @Override
            public void onError(String message) {
                onLoginError(message);
            }
        });
    }
    
    private void onLoginSuccess(Node node) {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("/id/krakatio/sonik/view/Menu.fxml"));
            Stage stage = (Stage) node.getScene().getWindow();
            stage.setScene(new Scene(root));
        } catch (IOException ex) {
            DialogBuilder.showError(ex.getMessage());
        }
    }
    
    private void onLoginError(String message) {
        DialogBuilder.showError(message);
    }

}
