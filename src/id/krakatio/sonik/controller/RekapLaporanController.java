/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.config.AppConfig;
import id.krakatio.sonik.manager.ICallback;
import id.krakatio.sonik.manager.LaporanManager;
import id.krakatio.sonik.model.Laporan;
import id.krakatio.sonik.util.DialogBuilder;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class RekapLaporanController implements Initializable {

    @FXML
    private TableColumn<Laporan, String> hari;
    @FXML
    private TableColumn<Laporan, String> tanggal;
    @FXML
    private TableColumn<Laporan, String> jam;
    @FXML
    private TableColumn<Laporan, String> acara;
    @FXML
    private TableColumn<Laporan, String> tempat;
    @FXML
    private TableColumn<Laporan, String> pemimpinRapat;
    @FXML
    private TableColumn<Laporan, String> pesertaRapat;
    @FXML
    private TableColumn<Laporan, String> hasilRapat;
    @FXML
    private TableView<Laporan> rekapLaporan;
    @FXML
    private TextField searchRekapLaporan;

    private LaporanManager laporanManager;
    private List<Laporan> laporanList;
    private ObservableList<Laporan> tableData;
    private Laporan selectedLaporan;
    @FXML
    private BorderPane root;
    
    /**
     * Initializes the controller class.
     */    

    @FXML
    private void detailLaporan(MouseEvent event) throws IOException {
        if (selectedLaporan != null) {
            FXMLLoader loader = new FXMLLoader(
                    getClass().getResource(AppConfig.APP_PACKAGE+"view/DetailLaporan.fxml")
            );

            Parent parent = loader.load();
            DetailLaporanController controller = (DetailLaporanController) loader.getController();
            controller.setLaporan(selectedLaporan);
            
            root.setCenter(parent);
        }
    }

    @FXML
    private void actionSearchLaporan(ActionEvent event) {
        String query = searchRekapLaporan.getText();
        tableData.clear();
        if (query == null || query.trim().isEmpty()) {
            tableData.addAll(laporanList);
        } else {
            for (Laporan b : laporanList) {
                if (b.contains(query)) tableData.add(b);
            }
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        tableData = FXCollections.observableArrayList();
        laporanManager = LaporanManager.getInstance();
        
        hari.setCellValueFactory(value -> value.getValue().hariProperty());
        tanggal.setCellValueFactory(value -> value.getValue().tanggalProperty());
        jam.setCellValueFactory(value -> value.getValue().jamProperty());
        acara.setCellValueFactory(value -> value.getValue().acaraProperty());
        tempat.setCellValueFactory(value -> value.getValue().tempatProperty());
        pemimpinRapat.setCellValueFactory(value -> value.getValue().pemimpinProperty());
        pesertaRapat.setCellValueFactory(value -> value.getValue().pesertaProperty());
        hasilRapat.setCellValueFactory(value -> value.getValue().hasilProperty());
        
        rekapLaporan.setItems(tableData);
        rekapLaporan.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Laporan> observable, 
                        Laporan oldValue, 
                        Laporan newValue) -> {
            selectedLaporan = newValue;            
        });
        
        laporanManager.getLaporanList(new ICallback<List<Laporan>>() {
            @Override
            public void onSuccess(List<Laporan> data) {
                laporanList = data;
                tableData.clear();
                tableData.addAll(laporanList);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }            
        });
    }    

    @FXML
    private void btnPrint(MouseEvent event) {
    }
    
}
