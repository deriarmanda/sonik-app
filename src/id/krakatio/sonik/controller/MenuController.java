/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.controller;

import id.krakatio.sonik.config.AppConfig;
import id.krakatio.sonik.manager.UserManager;
import id.krakatio.sonik.model.User;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class MenuController implements Initializable {

    @FXML
    private BorderPane borderpane;
    @FXML
    private ImageView fotoProfile;
    @FXML
    private Text userName;
//    @FXML
//    private TextField pencarianArsip;
    @FXML
    private Text title;

    private UserManager userManager;
    @FXML
    private TextField searchMenu;
    @FXML
    private HBox pencarianHilang;
    
    /**
     * Initializes the controller class.
     */
    
    @FXML
    private void Baru(MouseEvent event) {
        title.setText("Surat Baru");
        pencarianHilang.setVisible(true);
        loadUi("Input");
    }

    @FXML
    private void detailSurat(MouseEvent event) {
        title.setText("Detail Surat");
        pencarianHilang.setVisible(true);
        loadUi("DetailSurat");
    }

    @FXML
    private void infoPinjam(MouseEvent event) {
        title.setText("Info Peminjaman");
        pencarianHilang.setVisible(true);
        loadUi("InfoPinjam");
    }

    @FXML
    private void riwayatPinjam(MouseEvent event) {
        title.setText("Riwayat Peminjaman");
        pencarianHilang.setVisible(true);
        loadUi("RiwayatPinjam");
    }

    @FXML
    private void rekapSuratMasuk(MouseEvent event) {
        title.setText("Rekap Surat Masuk");
        pencarianHilang.setVisible(true);
        loadUi("RekapSuratMasuk");
    }

    @FXML
    private void rekapSuratKeluar(MouseEvent event) {
        title.setText("Rekap Surat Keluar");
        pencarianHilang.setVisible(true);
        loadUi("RekapSuratKeluar");
    }

    @FXML
    private void rekapBukuAgenda(MouseEvent event) {
        title.setText("Rekap Buku Agenda");
        pencarianHilang.setVisible(true);
        loadUi("RekapBukuAgenda");
    }

    @FXML
    private void rekapPinjamArsip(MouseEvent event) {
        title.setText("Rekap Peminjaman Arsip");
        pencarianHilang.setVisible(true);
        loadUi("RekapPinjamArsip");
    }

    @FXML
    private void rekapLaporan(MouseEvent event) {
        title.setText("Rekap Laporan");
        pencarianHilang.setVisible(false);
        loadUi("RekapLaporan");
    }

    @FXML
    private void formulirBaru(MouseEvent event) {
        title.setText("Formulir Baru");
        pencarianHilang.setVisible(false);
        loadUi("FormulirBaru");
    }

    @FXML
    private void rekapFormulir(MouseEvent event) {
        title.setText("Rekap Formulir");
        pencarianHilang.setVisible(false);
        loadUi("RekapFormulir");
    }

    @FXML
    private void laporanBaru(MouseEvent event) {
        title.setText("Laporan Baru");
        pencarianHilang.setVisible(false);
        loadUi("laporanBaru");
    }

    @FXML
    private void Home(MouseEvent event) {
        title.setText("Home");
        pencarianHilang.setVisible(true);
        loadUi("Home");
    }

    @FXML
    private void pengaturan(MouseEvent event) {
        title.setText("Pengaturan");
        pencarianHilang.setVisible(true);
        loadUi("Pengaturan");
    }

    @FXML
    private void tentang(MouseEvent event) {
        title.setText("Tentang Aplikasi");
        pencarianHilang.setVisible(true);
        loadUi("Tentang");
    }
    
    @FXML
    private void logout(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/id/krakatio/sonik/view/Login.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        userManager = UserManager.getInstance();
        User user = userManager.getActiveUser();
        userName.setText(user.getFullName());
        if (user.getImagePath() != null) {
            File profile = new File(user.getImagePath());
        
            fotoProfile.setImage(
                    new Image(profile.toURI().toString(), 
                    fotoProfile.getFitWidth(),
                    fotoProfile.getFitHeight(),
                    false, false
            ));
        }  
        
        title.setText("Home");
        loadUi("Home");
    }
    private void loadUi(String ui){
        Parent root = null;
        try{
            root = FXMLLoader.load(getClass().getResource(AppConfig.APP_PACKAGE+"view/"+ui+".fxml"));
        } catch (IOException ex) {
            Logger.getLogger(id.krakatio.sonik.controller.MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        borderpane.setCenter(root);
    }

    @FXML
    private void actionSearchMenu(ActionEvent event) {
    }
    
}
