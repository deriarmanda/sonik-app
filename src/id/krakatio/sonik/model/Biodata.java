/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import id.krakatio.sonik.persister.SimpleStringPropertyPersister;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Deri Armanda
 */
@DatabaseTable(tableName = "biodata")
public class Biodata {
    
    public static final String NAMA_LENGKAP_FIELD_NAME = "nama_lengkap";
    public static final String NAMA_PANGGILAN_FIELD_NAME = "nama_panggilan";
    public static final String JENIS_KELAMIN_FIELD_NAME = "jenis_kelamin";
    public static final String TANGGAL_MULAI_KERJA_FIELD_NAME = "tanggal_mulai_kerja";
    public static final String JABATAN_FIELD_NAME = "jabatan";
    public static final String DIVISI_FIELD_NAME = "divisi";
    public static final String NIP_FIELD_NAME = "nip";
    public static final String AREA_KERJA_FIELD_NAME = "area_kerja";
    public static final String ALAMAT_KTP_FIELD_NAME = "alamat_ktp";
    public static final String ALAMAT_SEKARANG_FIELD_NAME = "alamat_sekarang";
    public static final String TELEPON_RUMAH_FIELD_NAME = "telepon_rumah";
    public static final String TELEPON_HP_FIELD_NAME = "telepon_hp";
    public static final String TEMPAT_LAHIR_FIELD_NAME = "tempat_lahir";
    public static final String TANGGAL_LAHIR_FIELD_NAME = "tanggal_lahir";
    public static final String USIA_FIELD_NAME = "usia";
    public static final String PENDIDIKAN_TERAKHIR_FIELD_NAME = "pendidikan_terakhir";
    public static final String SEKOLAH_FIELD_NAME = "sekolah";
    public static final String BANK_FIELD_NAME = "bank";
    public static final String NOMOR_REKENING_FIELD_NAME = "nomor_rekening";
    public static final String NOMOR_KTP_FIELD_NAME = "nomor_ktp";
    public static final String JAMSOSTEK_FIELD_NAME = "jamsostek";
    public static final String NPWP_FIELD_NAME = "npwp";
    public static final String SIM_C_STATUS_FIELD_NAME = "sim_c_status";
    public static final String SIM_C_BERLAKU_FIELD_NAME = "sim_c_berlaku";
    public static final String SIM_A_STATUS_FIELD_NAME = "sim_a_status";
    public static final String SIM_A_BERLAKU_FIELD_NAME = "sim_a_berlaku";
    public static final String SIM_B1_STATUS_FIELD_NAME = "sim_b1_status";
    public static final String SIM_B1_BERLAKU_FIELD_NAME = "sim_b1_berlaku";
    public static final String SIM_B1_UMUM_STATUS_FIELD_NAME = "sim_b1_umum_status";
    public static final String SIM_B1_UMUM_BERLAKU_FIELD_NAME = "sim_b1_umum_berlaku";
    public static final String SIM_B2_STATUS_FIELD_NAME = "sim_b2_status";
    public static final String SIM_B2_BERLAKU_FIELD_NAME = "sim_b2_berlaku";
    public static final String STATUS_PERKAWINAN_FIELD_NAME = "status_perkawinan";
    public static final String NAMA_ISTRI_FIELD_NAME = "nama_istri";
    public static final String TEMPAT_LAHIR_ISTRI_FIELD_NAME = "tempat_lahir_istri";
    public static final String TANGGAL_LAHIR_ISTRI_FIELD_NAME = "tanggal_lahir_istri";
    public static final String TANGGAL_MENIKAH_FIELD_NAME = "tanggal_menikah";
    public static final String USIA_ISTRI_FIELD_NAME = "usia_istri";
    public static final String IMAGE_PATH_FIELD_NAME = "image_path";
    
    @DatabaseField(generatedId = true) 
    private int uid;
    
    @DatabaseField(
            columnName = NAMA_LENGKAP_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaLengkap;
    
    @DatabaseField(
            columnName = NAMA_PANGGILAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaPanggilan;
    
    @DatabaseField(
            columnName = JENIS_KELAMIN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty jenisKelamin;
    
    @DatabaseField(
            columnName = TANGGAL_MULAI_KERJA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tanggalMulaiKerja;
    
    @DatabaseField(
            columnName = JABATAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty jabatan;
    
    @DatabaseField(
            columnName = DIVISI_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty divisi;
    
    @DatabaseField(
            columnName = NIP_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nip;
    
    @DatabaseField(
            columnName = AREA_KERJA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty areaKerja;
    
    @DatabaseField(
            columnName = ALAMAT_KTP_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty alamatKtp;
    
    @DatabaseField(
            columnName = ALAMAT_SEKARANG_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty alamatSekarang;
    
    @DatabaseField(
            columnName = TELEPON_RUMAH_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty teleponRumah;
    
    @DatabaseField(
            columnName = TELEPON_HP_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty teleponHp;
    
    @DatabaseField(
            columnName = TEMPAT_LAHIR_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tempatLahir;
    
    @DatabaseField(
            columnName = TANGGAL_LAHIR_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tanggalLahir;
    
    @DatabaseField(
            columnName = USIA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty usia;
    
    @DatabaseField(
            columnName = PENDIDIKAN_TERAKHIR_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty pendidikanTerakhir;
    
    @DatabaseField(
            columnName = SEKOLAH_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty sekolah;
    
    @DatabaseField(
            columnName = BANK_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty bank;
    
    @DatabaseField(
            columnName = NOMOR_REKENING_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nomorRekening;
    
    @DatabaseField(
            columnName = NOMOR_KTP_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nomorKtp;
    
    @DatabaseField(
            columnName = JAMSOSTEK_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty jamsostek;
    
    @DatabaseField(
            columnName = NPWP_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty npwp;
    
    @DatabaseField(
            columnName = SIM_C_STATUS_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty simCStatus;
    
    @DatabaseField(
            columnName = SIM_C_BERLAKU_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty simCBerlaku;
    
    @DatabaseField(
            columnName = SIM_A_STATUS_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty simAStatus;
    
    @DatabaseField(
            columnName = SIM_A_BERLAKU_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty simABerlaku;
    
    @DatabaseField(
            columnName = SIM_B1_STATUS_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty simB1Status;
    
    @DatabaseField(
            columnName = SIM_B1_BERLAKU_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty simB1Berlaku;
    
    @DatabaseField(
            columnName = SIM_B1_UMUM_STATUS_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty simB1UmumStatus;
    
    @DatabaseField(
            columnName = SIM_B1_UMUM_BERLAKU_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty simB1UmumBerlaku;
    
    @DatabaseField(
            columnName = SIM_B2_STATUS_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty simB2Status;
    
    @DatabaseField(
            columnName = SIM_B2_BERLAKU_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty simB2Berlaku;
    
    @DatabaseField(
            columnName = STATUS_PERKAWINAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty statusPerkawinan;
    
    @DatabaseField(
            columnName = NAMA_ISTRI_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaIstri;
    
    @DatabaseField(
            columnName = TEMPAT_LAHIR_ISTRI_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tempatLahirIstri;
    
    @DatabaseField(
            columnName = TANGGAL_LAHIR_ISTRI_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tanggalLahirIstri;
    
    @DatabaseField(
            columnName = TANGGAL_MENIKAH_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tanggalMenikah;
    
    @DatabaseField(
            columnName = USIA_ISTRI_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty usiaIstri;
    
    @DatabaseField(
            columnName = IMAGE_PATH_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty imagePath;
    
    private List<BiodataAnak> listAnak;

    public Biodata() { }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getNamaLengkap() {
        return namaLengkap.get();
    }

    public void setNamaLengkap(String namaLengkap) {
        if (this.namaLengkap == null) this.namaLengkap = new SimpleStringProperty(namaLengkap);
        else this.namaLengkap.set(namaLengkap);
    }
    public StringProperty namaLengkapProperty() {
        return namaLengkap;
    }

    public String getNamaPanggilan() {
        return namaPanggilan.get();
    }

    public void setNamaPanggilan(String namaPanggilan) {
        if (this.namaPanggilan == null) this.namaPanggilan = new SimpleStringProperty(namaPanggilan);
        else this.namaPanggilan.set(namaPanggilan);
    }
    public StringProperty namaPanggilanProperty() {
        return namaPanggilan;
    }

    public String getJenisKelamin() {
        return jenisKelamin.get();
    }

    public void setJenisKelamin(String jenisKelamin) {
        if (this.jenisKelamin == null) this.jenisKelamin = new SimpleStringProperty(jenisKelamin);
        else this.jenisKelamin.set(jenisKelamin);
    }
    public StringProperty jenisKelaminProperty() {
        return jenisKelamin;
    }

    public String getTanggalMulaiKerja() {
        return tanggalMulaiKerja.get();
    }

    public void setTanggalMulaiKerja(String tanggalMulaiKerja) {
        if (this.tanggalMulaiKerja == null) this.tanggalMulaiKerja = new SimpleStringProperty(tanggalMulaiKerja);
        else this.tanggalMulaiKerja.set(tanggalMulaiKerja);
    }
    public StringProperty tanggalMulaiKerjaProperty() {
        return tanggalMulaiKerja;
    }

    public String getJabatan() {
        return jabatan.get();
    }

    public void setJabatan(String jabatan) {
        if (this.jabatan == null) this.jabatan = new SimpleStringProperty(jabatan);
        else this.jabatan.set(jabatan);
    }
    public StringProperty jabatanProperty() {
        return jabatan;
    }

    public String getDivisi() {
        return divisi.get();
    }

    public void setDivisi(String divisi) {
        if (this.divisi == null) this.divisi = new SimpleStringProperty(divisi);
        else this.divisi.set(divisi);
    }
    public StringProperty divisiProperty() {
        return divisi;
    }

    public String getNip() {
        return nip.get();
    }

    public void setNip(String nip) {
        if (this.nip == null) this.nip = new SimpleStringProperty(nip);
        else this.nip.set(nip);
    }
    public StringProperty nipProperty() {
        return nip;
    }

    public String getAreaKerja() {
        return areaKerja.get();
    }

    public void setAreaKerja(String areaKerja) {
        if (this.areaKerja == null) this.areaKerja = new SimpleStringProperty(areaKerja);
        else this.areaKerja.set(areaKerja);
    }
    public StringProperty areaKerjaProperty() {
        return areaKerja;
    }

    public String getAlamatKtp() {
        return alamatKtp.get();
    }

    public void setAlamatKtp(String alamatKtp) {
        if (this.alamatKtp == null) this.alamatKtp = new SimpleStringProperty(alamatKtp);
        else this.alamatKtp.set(alamatKtp);
    }
    public StringProperty alamatKtpProperty() {
        return alamatKtp;
    }

    public String getAlamatSekarang() {
        return alamatSekarang.get();
    }

    public void setAlamatSekarang(String alamatSekarang) {
        if (this.alamatSekarang == null) this.alamatSekarang = new SimpleStringProperty(alamatSekarang);
        else this.alamatSekarang.set(alamatSekarang);
    }
    public StringProperty alamatSekarangProperty() {
        return alamatSekarang;
    }

    public String getTeleponRumah() {
        return teleponRumah.get();
    }

    public void setTeleponRumah(String teleponRumah) {
        if (this.teleponRumah == null) this.teleponRumah = new SimpleStringProperty(teleponRumah);
        else this.teleponRumah.set(teleponRumah);
    }
    public StringProperty teleponRumahProperty() {
        return teleponRumah;
    }

    public String getTeleponHp() {
        return teleponHp.get();
    }

    public void setTeleponHp(String teleponHp) {
        if (this.teleponHp == null) this.teleponHp = new SimpleStringProperty(teleponHp);
        else this.teleponHp.set(teleponHp);
    }
    public StringProperty teleponHpProperty() {
        return teleponHp;
    }

    public String getTempatLahir() {
        return tempatLahir.get();
    }

    public void setTempatLahir(String tempatLahir) {
        if (this.tempatLahir == null) this.tempatLahir = new SimpleStringProperty(tempatLahir);
        else this.tempatLahir.set(tempatLahir);
    }
    public StringProperty tempatLahirProperty() {
        return tempatLahir;
    }

    public String getTanggalLahir() {
        return tanggalLahir.get();
    }

    public void setTanggalLahir(String tanggalLahir) {
        if (this.tanggalLahir == null) this.tanggalLahir = new SimpleStringProperty(tanggalLahir);
        else this.tanggalLahir.set(tanggalLahir);
    }
    public StringProperty tanggalLahirProperty() {
        return tanggalLahir;
    }

    public String getUsia() {
        return usia.get();
    }

    public void setUsia(String usia) {
        if (this.usia == null) this.usia = new SimpleStringProperty(usia);
        else this.usia.set(usia);
    }
    public StringProperty usiaProperty() {
        return usia;
    }

    public String getPendidikanTerakhir() {
        return pendidikanTerakhir.get();
    }

    public void setPendidikanTerakhir(String pendidikanTerakhir) {
        if (this.pendidikanTerakhir == null) this.pendidikanTerakhir = new SimpleStringProperty(pendidikanTerakhir);
        else this.pendidikanTerakhir.set(pendidikanTerakhir);
    }
    public StringProperty pendidikanTerakhirProperty() {
        return pendidikanTerakhir;
    }

    public String getSekolah() {
        return sekolah.get();
    }

    public void setSekolah(String sekolah) {
        if (this.sekolah == null) this.sekolah = new SimpleStringProperty(sekolah);
        else this.sekolah.set(sekolah);
    }
    public StringProperty sekolahProperty() {
        return sekolah;
    }

    public String getBank() {
        return bank.get();
    }

    public void setBank(String bank) {
        if (this.bank == null) this.bank = new SimpleStringProperty(bank);
        else this.bank.set(bank);
    }
    public StringProperty bankProperty() {
        return bank;
    }

    public String getNomorRekening() {
        return nomorRekening.get();
    }

    public void setNomorRekening(String nomorRekening) {
        if (this.nomorRekening == null) this.nomorRekening = new SimpleStringProperty(nomorRekening);
        else this.nomorRekening.set(nomorRekening);
    }
    public StringProperty nomorRekeningProperty() {
        return nomorRekening;
    }

    public String getNomorKtp() {
        return nomorKtp.get();
    }

    public void setNomorKtp(String nomorKtp) {
        if (this.nomorKtp == null) this.nomorKtp = new SimpleStringProperty(nomorKtp);
        else this.nomorKtp.set(nomorKtp);
    }
    public StringProperty nomorKtpProperty() {
        return nomorKtp;
    }

    public String getJamsostek() {
        return jamsostek.get();
    }

    public void setJamsostek(String jamsostek) {
        if (this.jamsostek == null) this.jamsostek = new SimpleStringProperty(jamsostek);
        else this.jamsostek.set(jamsostek);
    }
    public StringProperty jamsostekProperty() {
        return jamsostek;
    }

    public String getNpwp() {
        return npwp.get();
    }

    public void setNpwp(String npwp) {
        if (this.npwp == null) this.npwp = new SimpleStringProperty(npwp);
        else this.npwp.set(npwp);
    }
    public StringProperty npwpProperty() {
        return npwp;
    }

    public String getSimCStatus() {
        return simCStatus.get();
    }

    public void setSimCStatus(String simCStatus) {
        if (this.simCStatus == null) this.simCStatus = new SimpleStringProperty(simCStatus);
        else this.simCStatus.set(simCStatus);
    }
    public StringProperty simCStatusProperty() {
        return simCStatus;
    }

    public String getSimCBerlaku() {
        return simCBerlaku.get();
    }

    public void setSimCBerlaku(String simCBerlaku) {
        if (this.simCBerlaku == null) this.simCBerlaku = new SimpleStringProperty(simCBerlaku);
        else this.simCBerlaku.set(simCBerlaku);
    }
    public StringProperty simCBerlakuProperty() {
        return simCBerlaku;
    }

    public String getSimAStatus() {
        return simAStatus.get();
    }

    public void setSimAStatus(String simAStatus) {
        if (this.simAStatus == null) this.simAStatus = new SimpleStringProperty(simAStatus);
        else this.simAStatus.set(simAStatus);
    }
    public StringProperty simAStatusProperty() {
        return simAStatus;
    }

    public String getSimABerlaku() {
        return simABerlaku.get();
    }

    public void setSimABerlaku(String simABerlaku) {
        if (this.simABerlaku == null) this.simABerlaku = new SimpleStringProperty(simABerlaku);
        else this.simABerlaku.set(simABerlaku);
    }
    public StringProperty simABerlakuProperty() {
        return simABerlaku;
    }

    public String getSimB1Status() {
        return simB1Status.get();
    }

    public void setSimB1Status(String simB1Status) {
        if (this.simB1Status == null) this.simB1Status = new SimpleStringProperty(simB1Status);
        else this.simB1Status.set(simB1Status);
    }
    public StringProperty simB1StatusProperty() {
        return simB1Status;
    }

    public String getSimB1Berlaku() {
        return simB1Berlaku.get();
    }

    public void setSimB1Berlaku(String simB1Berlaku) {
        if (this.simB1Berlaku == null) this.simB1Berlaku = new SimpleStringProperty(simB1Berlaku);
        else this.simB1Berlaku.set(simB1Berlaku);
    }
    public StringProperty simB1BerlakuProperty() {
        return simB1Berlaku;
    }

    public String getSimB1UmumStatus() {
        return simB1UmumStatus.get();
    }

    public void setSimB1UmumStatus(String simB1UmumStatus) {
        if (this.simB1UmumStatus == null) this.simB1UmumStatus = new SimpleStringProperty(simB1UmumStatus);
        else this.simB1UmumStatus.set(simB1UmumStatus);
    }
    public StringProperty simB1UmumStatusProperty() {
        return simB1UmumStatus;
    }

    public String getSimB1UmumBerlaku() {
        return simB1UmumBerlaku.get();
    }

    public void setSimB1UmumBerlaku(String simB1UmumBerlaku) {
        if (this.simB1UmumBerlaku == null) this.simB1UmumBerlaku = new SimpleStringProperty(simB1UmumBerlaku);
        else this.simB1UmumBerlaku.set(simB1UmumBerlaku);
    }
    public StringProperty simB1UmumBerlakuProperty() {
        return simB1UmumBerlaku;
    }

    public String getSimB2Status() {
        return simB2Status.get();
    }

    public void setSimB2Status(String simB2Status) {
        if (this.simB2Status == null) this.simB2Status = new SimpleStringProperty(simB2Status);
        else this.simB2Status.set(simB2Status);
    }
    public StringProperty simB2StatusProperty() {
        return simB2Status;
    }

    public String getSimB2Berlaku() {
        return simB2Berlaku.get();
    }

    public void setSimB2Berlaku(String simB2Berlaku) {
        if (this.simB2Berlaku == null) this.simB2Berlaku = new SimpleStringProperty(simB2Berlaku);
        else this.simB2Berlaku.set(simB2Berlaku);
    }
    public StringProperty simB2BerlakuProperty() {
        return simB2Berlaku;
    }

    public String getStatusPerkawinan() {
        return statusPerkawinan.get();
    }

    public void setStatusPerkawinan(String statusPerkawinan) {
        if (this.statusPerkawinan == null) this.statusPerkawinan = new SimpleStringProperty(statusPerkawinan);
        else this.statusPerkawinan.set(statusPerkawinan);
    }
    public StringProperty statusPerkawinanProperty() {
        return statusPerkawinan;
    }

    public String getNamaIstri() {
        return namaIstri.get();
    }

    public void setNamaIstri(String namaIstri) {
        if (this.namaIstri == null) this.namaIstri = new SimpleStringProperty(namaIstri);
        else this.namaIstri.set(namaIstri);
    }
    public StringProperty namaIstriProperty() {
        return namaIstri;
    }

    public String getTempatLahirIstri() {
        return tempatLahirIstri.get();
    }

    public void setTempatLahirIstri(String tempatLahirIstri) {
        if (this.tempatLahirIstri == null) this.tempatLahirIstri = new SimpleStringProperty(tempatLahirIstri);
        else this.tempatLahirIstri.set(tempatLahirIstri);
    }
    public StringProperty tempatLahirIstriProperty() {
        return tempatLahirIstri;
    }

    public String getTanggalLahirIstri() {
        return tanggalLahirIstri.get();
    }

    public void setTanggalLahirIstri(String tanggalLahirIstri) {
        if (this.tanggalLahirIstri == null) this.tanggalLahirIstri = new SimpleStringProperty(tanggalLahirIstri);
        else this.tanggalLahirIstri.set(tanggalLahirIstri);
    }
    public StringProperty tanggalLahirIstriProperty() {
        return tanggalLahirIstri;
    }

    public String getTanggalMenikah() {
        return tanggalMenikah.get();
    }

    public void setTanggalMenikah(String tanggalMenikah) {
        if (this.tanggalMenikah == null) this.tanggalMenikah = new SimpleStringProperty(tanggalMenikah);
        else this.tanggalMenikah.set(tanggalMenikah);
    }
    public StringProperty tanggalMenikahProperty() {
        return tanggalMenikah;
    }

    public String getUsiaIstri() {
        return usiaIstri.get();
    }

    public void setUsiaIstri(String usiaIstri) {
        if (this.usiaIstri == null) this.usiaIstri = new SimpleStringProperty(usiaIstri);
        else this.usiaIstri.set(usiaIstri);
    }
    public StringProperty usiaIstriProperty() {
        return usiaIstri;
    }

    public String getImagePath() {
        return imagePath.get();
    }

    public void setImagePath(String imagePath) {
        if (this.imagePath == null) this.imagePath = new SimpleStringProperty(imagePath);
        else this.imagePath.set(imagePath);
    }
    public StringProperty imagePathProperty() {
        return imagePath;
    }

    public List<BiodataAnak> getListAnak() {
        return listAnak;
    }

    public void setListAnak(List<BiodataAnak> listAnak) {
        this.listAnak = listAnak;
    }
    
    public boolean contains(String value) {
        value = value.toLowerCase();
        return getNamaLengkap().toLowerCase().contains(value) ||
                getNamaPanggilan().toLowerCase().contains(value) ||
                getJenisKelamin().toLowerCase().contains(value) ||
                getTanggalMulaiKerja().toLowerCase().contains(value) ||
                getJabatan().toLowerCase().contains(value) ||
                getDivisi().toLowerCase().contains(value)||
                getNip().toLowerCase().contains(value)||
                getAreaKerja().toLowerCase().contains(value)||
                getAlamatKtp().toLowerCase().contains(value)||
                getAlamatSekarang().toLowerCase().contains(value)||
                getTeleponRumah().toLowerCase().contains(value)||
                getTeleponHp().toLowerCase().contains(value)||
                getTempatLahir().toLowerCase().contains(value)||
                getTanggalLahir().toLowerCase().contains(value)||
                getUsia().toLowerCase().contains(value)||
                getPendidikanTerakhir().toLowerCase().contains(value)||
                getSekolah().toLowerCase().contains(value)||
                getBank().toLowerCase().contains(value)||
                getNomorRekening().toLowerCase().contains(value)||
                getNomorKtp().toLowerCase().contains(value)||
                getJamsostek().toLowerCase().contains(value)||
                getNpwp().toLowerCase().contains(value)||
                getStatusPerkawinan().toLowerCase().contains(value)||
                getNamaIstri().toLowerCase().contains(value)||
                getTempatLahirIstri().toLowerCase().contains(value)||
                getTanggalLahirIstri().toLowerCase().contains(value)||
                getTanggalMenikah().toLowerCase().contains(value)||
                getUsiaIstri().toLowerCase().contains(value);
    }
}
