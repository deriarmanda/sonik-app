/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import id.krakatio.sonik.persister.SimpleStringPropertyPersister;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Deri Armanda
 */
@DatabaseTable(tableName = "biodata_anak")
public class BiodataAnak {
    
    public static final String UID_ORANG_TUA_FIELD_NAME = "uid_orang_tua";
    public static final String NAMA_FIELD_NAME = "nama_anak";
    public static final String JENIS_KELAMIN_PINJAM_FIELD_NAME = "jenis_kelamin";
    public static final String USIA_FIELD_NAME = "usia";
    public static final String PENDIDIKAN_PEMINJAM_FIELD_NAME = "pendidikan";
    public static final String PEKERJAAN_FIELD_NAME = "pekerjaan";
        
    @DatabaseField(generatedId = true) 
    private int uid;
    
    @DatabaseField(
            canBeNull = false, 
            foreign = true,
            columnName = UID_ORANG_TUA_FIELD_NAME)
    private Biodata biodataOrangTua;
    
    @DatabaseField(
            columnName = NAMA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nama;
    
    @DatabaseField(
            columnName = JENIS_KELAMIN_PINJAM_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty jenisKelamin;
    
    @DatabaseField(
            columnName = USIA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty usia;
    
    @DatabaseField(
            columnName = PENDIDIKAN_PEMINJAM_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty pendidikan;
    
    @DatabaseField(
            columnName = PEKERJAAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty pekerjaan;
    
    public BiodataAnak() { }
        
    public int getUid() {
        return uid;
    }
    public void setUid(int uid) {
        this.uid = uid;
    }

    public Biodata getBiodataOrangTua() {
        return biodataOrangTua;
    }

    public void setBiodataOrangTua(Biodata biodataOrangTua) {
        this.biodataOrangTua = biodataOrangTua;
    }
    
    public String getNama() {
        return nama.get();
    }
    public void setNama(String nama) {
        if (this.nama == null) this.nama = new SimpleStringProperty(nama);
        else this.nama.set(nama);
    }
    public StringProperty namaProperty() {
        return nama;
    }
    
    public String getJenisKelamin() {
        return jenisKelamin.get();
    }
    public void setJenisKelamin(String jenisKelamin) {
        if (this.jenisKelamin == null) this.jenisKelamin = new SimpleStringProperty(jenisKelamin);
        else this.jenisKelamin.set(jenisKelamin);
    }
    public StringProperty jenisKelaminProperty() {
        return jenisKelamin;
    }
    
    public String getUsia() {
        return usia.get();
    }
    public void setUsia(String usia) {
        if (this.usia == null) this.usia = new SimpleStringProperty(usia);
        else this.usia.set(usia);
    }
    public StringProperty usiaProperty() {
        return usia;
    }
    
    public String getPendidikan() {
        return pendidikan.get();
    }
    public void setPendidikan(String pendidikan) {
        if (this.pendidikan == null) this.pendidikan = new SimpleStringProperty(pendidikan);
        else this.pendidikan.set(pendidikan);
    }
    public StringProperty pendidikanProperty() {
        return pendidikan;
    }
    
    public String getPekerjaan() {
        return pekerjaan.get();
    }
    public void setPekerjaan(String pekerjaan) {
        if (this.pekerjaan == null) this.pekerjaan = new SimpleStringProperty(pekerjaan);
        else this.pekerjaan.set(pekerjaan);
    }
    public StringProperty pekerjaanProperty() {
        return pekerjaan;
    }
}
