/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import id.krakatio.sonik.persister.SimpleStringPropertyPersister;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Deri Armanda
 */
@DatabaseTable(tableName = "laporan")
public class Laporan {
    
    public static final String HARI_FIELD_NAME = "hari";
    public static final String TANGGAL_FIELD_NAME = "tanggal";
    public static final String JAM_FIELD_NAME = "jam";
    public static final String TEMPAT_FIELD_NAME = "tempat";
    public static final String ACARA_FIELD_NAME = "acara";
    public static final String PEMIMPIN_FIELD_NAME = "pemimpin";
    public static final String PESERTA_FIELD_NAME = "peserta";
    public static final String HASIL_FIELD_NAME = "hasil";
    
    @DatabaseField(generatedId = true) 
    private int uid;
    
    @DatabaseField(
            columnName = HARI_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty hari;
    
    @DatabaseField(
            columnName = TANGGAL_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tanggal;
    
    @DatabaseField(
            columnName = JAM_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty jam;
    
    @DatabaseField(
            columnName = TEMPAT_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tempat;
    
    @DatabaseField(
            columnName = ACARA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty acara;
    
    @DatabaseField(
            columnName = PEMIMPIN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty pemimpin;
    
    @DatabaseField(
            columnName = PESERTA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty peserta;
    
    @DatabaseField(
            columnName = HASIL_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty hasil;
       
    public Laporan() { }
    
    public int getUid() {
        return uid;
    }
    public void setUid(int uid) {
        this.uid = uid;
    }
    
    public String getHari() {
        return hari.get();
    }
    public void setHari(String hari) {
        if (this.hari == null) this.hari = new SimpleStringProperty(hari);
        else this.hari.set(hari);
    }
    public StringProperty hariProperty() {
        return hari;
    }
    
    public String getTanggal() {
        return tanggal.get();
    }
    public void setTanggal(String tanggal) {
        if (this.tanggal == null) this.tanggal = new SimpleStringProperty(tanggal);
        else this.tanggal.set(tanggal);
    }
    public StringProperty tanggalProperty() {
        return tanggal;
    }
    
    public String getJam() {
        return jam.get();
    }
    public void setJam(String jam) {
        if (this.jam == null) this.jam = new SimpleStringProperty(jam);
        else this.jam.set(jam);
    }
    public StringProperty jamProperty() {
        return jam;
    }
    
    public String getTempat() {
        return tempat.get();
    }
    public void setTempat(String tempat) {
        if (this.tempat == null) this.tempat = new SimpleStringProperty(tempat);
        else this.tempat.set(tempat);
    }
    public StringProperty tempatProperty() {
        return tempat;
    }
    
    public String getAcara() {
        return acara.get();
    }
    public void setAcara(String acara) {
        if (this.acara == null) this.acara = new SimpleStringProperty(acara);
        else this.acara.set(acara);
    }
    public StringProperty acaraProperty() {
        return acara;
    }
    
    public String getPemimpin() {
        return pemimpin.get();
    }
    public void setPemimpin(String pemimpin) {
        if (this.pemimpin == null) this.pemimpin = new SimpleStringProperty(pemimpin);
        else this.pemimpin.set(pemimpin);
    }
    public StringProperty pemimpinProperty() {
        return pemimpin;
    }
    
    public String getPeserta() {
        return peserta.get();
    }
    public void setPeserta(String peserta) {
        if (this.peserta == null) this.peserta = new SimpleStringProperty(peserta);
        else this.peserta.set(peserta);
    }
    public StringProperty pesertaProperty() {
        return peserta;
    }
    
    public String getHasil() {
        return hasil.get();
    }
    public void setHasil(String hasil) {
        if (this.hasil == null) this.hasil = new SimpleStringProperty(hasil);
        else this.hasil.set(hasil);
    }
    public StringProperty hasilProperty() {
        return hasil;
    }
    
    public boolean contains(String value) {
        value = value.toLowerCase();
        return getHari().toLowerCase().contains(value) ||
                getTanggal().toLowerCase().contains(value) ||
                getJam().toLowerCase().contains(value) ||
                getTempat().toLowerCase().contains(value) ||
                getAcara().toLowerCase().contains(value) ||
                getPemimpin().toLowerCase().contains(value) ||
                getPeserta().toLowerCase().contains(value);
    }   
}
