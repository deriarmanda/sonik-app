/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 *
 * @author Deri Armanda
 */
@DatabaseTable(tableName = "attachments")
public class Attachment {
    
    public static final String UID_SURAT_FIELD_NAME = "uid_surat";
    public static final String NAMA_FILE_FIELD_NAME = "nama_file";
    public static final String PATH_FILE_FIELD_NAME = "path_file";
    
    @DatabaseField(generatedId = true) 
    private int uid;
    
    @DatabaseField(columnName = UID_SURAT_FIELD_NAME)
    private int uidSurat;
    
    @DatabaseField(columnName = NAMA_FILE_FIELD_NAME) 
    private String namaFile;
    
    @DatabaseField(columnName = PATH_FILE_FIELD_NAME) 
    private String pathFile;
    
    public Attachment() { }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getUidSurat() {
        return uidSurat;
    }

    public void setUidSurat(int uidSurat) {
        this.uidSurat = uidSurat;
    }

    public String getNamaFile() {
        return namaFile;
    }

    public void setNamaFile(String namaFile) {
        this.namaFile = namaFile;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }    
    
}
