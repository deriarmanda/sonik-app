/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 *
 * @author Deri Armanda
 */
@DatabaseTable(tableName = "header")
public class Header {
    
    public static final String HEADER_1_FIELD_NAME = "header_1";
    public static final String HEADER_2_FIELD_NAME = "header_2";
    public static final String HEADER_3_FIELD_NAME = "header_3";
    public static final String HEADER_4_FIELD_NAME = "header_4";
    public static final String KONTAK_FIELD_NAME = "kontak";
    public static final String IMAGE_PATH_FIELD_NAME = "image_path";
    
    @DatabaseField(generatedId = true) 
    private int uid;
    
    @DatabaseField(columnName = HEADER_1_FIELD_NAME) 
    private String header1;
    
    @DatabaseField(columnName = HEADER_2_FIELD_NAME) 
    private String header2;
    
    @DatabaseField(columnName = HEADER_3_FIELD_NAME) 
    private String header3;
    
    @DatabaseField(columnName = HEADER_4_FIELD_NAME) 
    private String header4;
    
    @DatabaseField(columnName = KONTAK_FIELD_NAME) 
    private String kontak;
    
    @DatabaseField(columnName = IMAGE_PATH_FIELD_NAME) 
    private String imagePath;
    
    public Header() { }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getHeader1() {
        return header1;
    }

    public void setHeader1(String header1) {
        this.header1 = header1;
    }

    public String getHeader2() {
        return header2;
    }

    public void setHeader2(String header2) {
        this.header2 = header2;
    }

    public String getHeader3() {
        return header3;
    }

    public void setHeader3(String header3) {
        this.header3 = header3;
    }

    public String getHeader4() {
        return header4;
    }

    public void setHeader4(String header4) {
        this.header4 = header4;
    }

    public String getKontak() {
        return kontak;
    }

    public void setKontak(String kontak) {
        this.kontak = kontak;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
    
    
}