/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 *
 * @author Deri Armanda
 */
@DatabaseTable(tableName = "users")
public class User {
    
    public static final String USERNAME_FIELD_NAME = "username";
    public static final String EMAIL_FIELD_NAME = "email";
    public static final String PASSWORD_FIELD_NAME = "password";
    public static final String FIRST_NAME_FIELD_NAME = "first_name";
    public static final String LAST_NAME_FIELD_NAME = "last_name";
    public static final String IMAGE_PATH_FIELD_NAME = "image_path";
    
    @DatabaseField(generatedId = true) 
    private int uid;
    
    @DatabaseField(
            unique = true,
            columnName = USERNAME_FIELD_NAME
    ) private String username;
    
    @DatabaseField(
            unique = true,
            columnName = EMAIL_FIELD_NAME
    ) private String email;
    
    @DatabaseField(columnName = PASSWORD_FIELD_NAME) 
    private String password;
    
    @DatabaseField(columnName = FIRST_NAME_FIELD_NAME) 
    private String firstName;
    
    @DatabaseField(columnName = LAST_NAME_FIELD_NAME) 
    private String lastName;
    
    @DatabaseField(columnName = IMAGE_PATH_FIELD_NAME) 
    private String imagePath;
    
    public User() { }
    
    public int getUid() {
        return uid;
    }
    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
    
    public String getFullName() {
        return firstName + " " + lastName;
    }
    
}
