/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.krakatio.sonik;

import id.krakatio.sonik.config.AppConfig;
import id.krakatio.sonik.util.DialogBuilder;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Deri Armanda
 */
public class MainApp extends Application {
    
    @Override
    public void start(Stage primaryStage) throws IOException {
        DialogBuilder.init(primaryStage);
        
        Parent root = FXMLLoader.load(getClass().getResource("/id/krakatio/sonik/view/Login.fxml"));
        Scene scene = new Scene(root);        
        primaryStage.setTitle(AppConfig.APP_NAME);
        primaryStage.getIcons().add(AppConfig.APP_LOGO);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
